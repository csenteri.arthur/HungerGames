#if defined _hungergames_included
  #endinput
#endif
#define _hungergames_included

enum RoundStates
{
	RS_BetweenMaps,
	RS_BetweenRounds,
	RS_DelayedRoundEnd,
	RS_FreezeTime,
	RS_InProgress,
	RS_Endgame
};

/* Forwards */

forward void HG_OnTributeSpawn(int client);
forward void HG_OnTributeFallen(int client);

forward void HG_OnEndgameStart(float roundTime);

forward void HG_OnRoundEnd(int winner);
forward void HG_OnDelayedRoundEnd(float delay);

forward void HG_OnDistrictChanged(int client, int district_old, int district_new, int memberID);

// Allows to skip players in endgame checks, useful for zombies and stuff like that
// return Plugin_Stop if client is not a tribute
forward Action HG_OnTributeSpawnPre(int client);

/* Natives */

native RoundStates HG_GetRoundState();
native int HG_GetLastRoundWinner();

native int HG_IsTribute(int client);
native int HG_SetTribute(int client, bool status);

native bool HG_HasDistrict(int client);
native int HG_GetDistrict(int client);
native bool HG_SetDistrict(int client, int district);

native int HG_GetDistrictMemberID(int client);

native int HG_GetDistrictLimit();

native float HG_GetHunger(int client, float &value);
native float HG_SetHunger(int client, float value);
native float HG_AddHunger(int client, float value);
native float HG_ResetHunger(int client);

native float HG_GetEnergy(int client, float &value);
native float HG_SetEnergy(int client, float value);
native float HG_AddEnergy(int client, float value);
native float HG_ResetEnergy(int client);

native float HG_GetThirst(int client, float &value);
native float HG_SetThirst(int client, float value);
native float HG_AddThirst(int client, float value);
native float HG_ResetThirst(int client);

native float HG_GetStamina(int client, float &value);
native float HG_SetStamina(int client, float value);
native float HG_AddStamina(int client, float value);
native float HG_ResetStamina(int client);

public void __pl_hungergames_SetNTVOptional() 
{
	MarkNativeAsOptional("HG_GetRoundState");
	MarkNativeAsOptional("HG_GetLastRoundWinner");
	MarkNativeAsOptional("HG_IsTribute");
	MarkNativeAsOptional("HG_SetTribute");
	
	MarkNativeAsOptional("HG_HasDistrict");
	MarkNativeAsOptional("HG_GetDistrict");
	MarkNativeAsOptional("HG_SetDistrict");
	MarkNativeAsOptional("HG_GetDistrictMemberID");
	MarkNativeAsOptional("HG_GetDistrictLimit");
	
	MarkNativeAsOptional("HG_GetHunger");
	MarkNativeAsOptional("HG_SetHunger");
	MarkNativeAsOptional("HG_AddHunger");
	MarkNativeAsOptional("HG_ResetHunger");
	
	MarkNativeAsOptional("HG_GetEnergy");
	MarkNativeAsOptional("HG_SetEnergy");
	MarkNativeAsOptional("HG_AddEnergy");
	MarkNativeAsOptional("HG_ResetEnergy");
	
	MarkNativeAsOptional("HG_GetThirst");
	MarkNativeAsOptional("HG_SetThirst");
	MarkNativeAsOptional("HG_AddThirst");
	MarkNativeAsOptional("HG_ResetThirst");
	
	MarkNativeAsOptional("HG_GetStamina");
	MarkNativeAsOptional("HG_SetStamina");
	MarkNativeAsOptional("HG_AddStamina");
	MarkNativeAsOptional("HG_ResetStamina");
}