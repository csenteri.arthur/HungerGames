#define MAX_PARTYS 14

Handle g_aPartyMembers[MAX_PARTYS] = {null, ... };
Handle g_aEmptyDistricts = null;

int g_iParty[MAXPLAYERS + 1] = {-1, ...};
int g_iPartyIcon[MAXPLAYERS + 1] = {-1, ...};

void ResetPartys()
{
	LoopClients(i)
	{
		g_iParty[i] = -1;
		ResetPartyIcon(i);
	}
		
	for (int i = 0; i < MAX_PARTYS; i++)
	{
		if(g_aPartyMembers[i] == null)
			continue;
		
		CloseHandle(g_aPartyMembers[i]);
		g_aPartyMembers[i] = null;
	}
	
	if(g_aEmptyDistricts == null)
		g_aEmptyDistricts = CreateArray(1);
	else ClearArray(g_aEmptyDistricts);
	
	PushArrayCell(g_aEmptyDistricts, 1);
	PushArrayCell(g_aEmptyDistricts, 2);
	PushArrayCell(g_aEmptyDistricts, 3);
	PushArrayCell(g_aEmptyDistricts, 4);
	PushArrayCell(g_aEmptyDistricts, 5);
	PushArrayCell(g_aEmptyDistricts, 6);
	PushArrayCell(g_aEmptyDistricts, 7);
	PushArrayCell(g_aEmptyDistricts, 8);
	PushArrayCell(g_aEmptyDistricts, 9);
	PushArrayCell(g_aEmptyDistricts, 10);
	PushArrayCell(g_aEmptyDistricts, 11);
	PushArrayCell(g_aEmptyDistricts, 12);
}

int GetParty(int client)
{
	return g_iParty[client];
}

int GetPartyMemberCount(int party)
{
	return GetArraySize(g_aPartyMembers[party]);
}

bool CreateParty(int client, int client2)
{
	if(g_iParty[client] != -1)
		return false;
		
	if(g_iParty[client2] != -1)
		return false;
		
	if(GetArraySize(g_aEmptyDistricts) <= 0)
		return false;
	
	if(g_bPartyVIP && !Client_HasAdminFlags(client, g_iVIPflags) && !Client_HasAdminFlags(client2, g_iVIPflags))
	{
		CPrintToChat(client, "%T", "Party_Not_VIP", client, g_sPrefix);
		CPrintToChat(client2, "%T", "Party_Not_VIP", client, g_sPrefix);
		
		return false;
	}
		
	int index = GetRandomInt(0, GetArraySize(g_aEmptyDistricts)-1);
	int district = GetArrayCell(g_aEmptyDistricts, index);
	RemoveFromArray(g_aEmptyDistricts, index);
	
	if(g_aPartyMembers[district] != null)
		CloseHandle(g_aPartyMembers[district]);
	g_aPartyMembers[district] = CreateArray(1);
	
	int memberID = PushArrayCell(g_aPartyMembers[district], client);
	int memberID2 = PushArrayCell(g_aPartyMembers[district], client2);
	
	Call_StartForward(g_OnDistrictChanged);
	Call_PushCell(client);
	Call_PushCell(g_iParty[client]);
	Call_PushCell(district);
	Call_PushCell(memberID);
	Call_Finish();
	
	Call_StartForward(g_OnDistrictChanged);
	Call_PushCell(client2);
	Call_PushCell(g_iParty[client2]);
	Call_PushCell(district);
	Call_PushCell(memberID2);
	Call_Finish();
	
	g_iParty[client] = district;
	g_iParty[client2] = district;
	
	CreateIcon(client);
	CreateIcon(client2);
	
	char sName1[64];
	Format(sName1, sizeof(sName1), "%N", client);
	CPrintToChat(client2, "%T", "Party_Created", client, g_sPrefix, sName1, district);
	
	char sName2[64];
	Format(sName2, sizeof(sName2), "%N", client2);
	CPrintToChat(client, "%T", "Party_Created", client, g_sPrefix, sName2, district);
	
	return true;
}

bool JoinParty(int inviter, int client, int party)
{
	if(g_aPartyMembers[party] == null)
		return false;
	
	PushArrayCell(g_aPartyMembers[party], client);
	g_iParty[client] = party;
	
	CreateIcon(client);
	
	AnnounceJoin(inviter, client, g_iParty[client]);
	
	return true;
}

void AnnounceJoin(int inviter, int client, int party)
{
	int members = GetArraySize(g_aPartyMembers[party]);
	
	char sName1[64];
	Format(sName1, sizeof(sName1), "%N", client);
	
	char sName2[64];
	Format(sName2, sizeof(sName2), "%N", inviter);
	
	for (int i = 0; i < members; i++)
	{
		int member = GetArrayCell(g_aPartyMembers[party], i);
		
		if(member == client)
			CPrintToChat(member, "%T", "Party_Join", member, g_sPrefix, sName2, party);
		else CPrintToChat(member, "%T", "Party_Joined", member, g_sPrefix, sName1, party);
	}
}

void LeaveParty(int client)
{
	if(g_iParty[client] == -1)
	{
		ResetPartyIcon(client);
		return;
	}
	
	if(g_aPartyMembers[g_iParty[client]] == null)
	{
		g_iParty[client] = -1;
		ResetPartyIcon(client);
		return;
	}
		
	ResetPartyIcon(client);
		
	int members = GetArraySize(g_aPartyMembers[g_iParty[client]]);
	
	for (int i = 0; i < members; i++)
	{
		if(GetArrayCell(g_aPartyMembers[g_iParty[client]], i) != client)
			continue;
			
		if(members == 1)
		{
			CloseHandle(g_aPartyMembers[g_iParty[client]]);
			g_aPartyMembers[g_iParty[client]] = null;
		}
		else RemoveFromArray(g_aPartyMembers[g_iParty[client]], i);
		
		g_iParty[client] = -1;
		
		return;
	}
}

void ResetPartyIcon(int client)
{
	if(g_iPartyIcon[client] != -1 && IsValidEntity(g_iPartyIcon[client]))
		AcceptEntityInput(g_iPartyIcon[client], "Kill");
	
	g_iPartyIcon[client] = -1;
}

void CreateIcon(int client)
{
	ResetPartyIcon(client);
	
	char sClient[16];
	Format(sClient, 16, "client%d", client);
	DispatchKeyValue(client, "targetname", sClient);
	
	float fPos[3];
	GetClientAbsOrigin(client, fPos);
	fPos[2] += 90.0;
	
	int iEnt = CreateEntityByName("env_sprite");
	if(iEnt <= 0) 
		return;
	
	char sPath[PLATFORM_MAX_PATH];
	
	Format(sPath, sizeof(sPath), "sprites/hg/districts/drunken/district_%d_128.vmt", g_iParty[client]);
	DispatchKeyValue(iEnt, "model", sPath);
	
	DispatchKeyValue(iEnt, "spawnflags", "1");
	DispatchKeyValue(iEnt, "scale", "0.08");
	DispatchKeyValue(iEnt, "rendermode", "1");
	DispatchKeyValue(iEnt, "rendercolor", "255 255 255");
	DispatchSpawn(iEnt);
	
	TeleportEntity(iEnt, fPos, NULL_VECTOR, NULL_VECTOR);
	
	SetVariantString(sClient);
	AcceptEntityInput(iEnt, "SetParent", iEnt, iEnt, 0);
	
	g_iPartyIcon[client] = iEnt;
}

bool CheckParty(int client, int buttons)
{
	if(g_iPartyLimit > 1 && RoundStatus < RS_Endgame && buttons & IN_USE)
	{
		int target = GetClientAimTarget(client);
		if(Entity_IsPlayer(target) && (g_fPartyMaxDistance <= 0.0 || Entity_InRange(client, target, g_fPartyMaxDistance)) && ((GetClientButtons(target) & IN_USE) || IsFakeClient(target)))
		{
			BlockPrimaryTools(client, 1.0);
			
			if(RoundStatus != RS_InProgress)
				PrintHintText(client, "%T", "Party_Disabled", client);
			
			if(GetParty(client) == -1 && GetParty(target) == -1 )
			{
				if(!CreateParty(client, target))
					PrintHintText(client, "%T", "Party_OutOfDistricts", client);
			}
			else if(GetParty(client) == -1)
			{
				if(g_iPartyLimit <= GetPartyMemberCount(GetParty(target)))
					PrintHintText(client, "%T", "Party_MemberLimit", client, GetParty(target), g_iPartyLimit);
				else JoinParty(target, client, GetParty(target));
			}
			else if(GetParty(target) == -1)
			{
				if(g_iPartyLimit <= GetPartyMemberCount(GetParty(client)))
					PrintHintText(client, "%T", "Party_MemberLimit", client, GetParty(client), g_iPartyLimit);
				else JoinParty(client, target, GetParty(client));
			}
		}
	}
}

public int Native_HasDistrict(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	return GetParty(client) != -1;
}
public int Native_GetDistrict(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	return GetParty(client);
}

public int Native_SetDistrict(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	int district_new = GetNativeCell(2);
	
	// Force Leave Party System
	if(district_new == -1)
	{
		Call_StartForward(g_OnDistrictChanged);
		Call_PushCell(client);
		Call_PushCell(g_iParty[client]);
		Call_PushCell(-1);
		Call_PushCell(-1);
		Call_Finish();
		
		LeaveParty(client);
		ResetPartyIcon(client);
		
		return true;
	}
	
	// Random Empty Not possible
	if(district_new == -2 && GetArraySize(g_aEmptyDistricts) > 0)
	{
		int index = GetRandomInt(0, GetArraySize(g_aEmptyDistricts)-1);
		RemoveFromArray(g_aEmptyDistricts, index);
		
		district_new = GetArrayCell(g_aEmptyDistricts, index);
	}
	
	// Not full?
	if(district_new == -2)
	{
		for (int i = 0; i < MAX_PARTYS; i++)
		{
			if(g_aPartyMembers[i] != null && g_iPartyLimit <= GetPartyMemberCount(i))
				continue;
			
			district_new = i;
		}
	}
	
	// All districts are full
	if(district_new <= -1)
		return false;
	
	// Leave old party
	if(GetParty(client) != -1)
		LeaveParty(client);
	
	// Create array
	if(g_aPartyMembers[district_new] == null)
		g_aPartyMembers[district_new] = CreateArray(1);
	
	// Push client
	int memberID = PushArrayCell(g_aPartyMembers[district_new], client);
	
	// Push forward
	Call_StartForward(g_OnDistrictChanged);
	Call_PushCell(client);
	Call_PushCell(g_iParty[client]);
	Call_PushCell(district_new);
	Call_PushCell(memberID);
	Call_Finish();
	
	// Cache district
	g_iParty[client] = district_new;
	
	// Create Icon
	CreateIcon(client);
	
	return true;
}

public int Native_GetDistrictMemberID(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	
	if(g_iParty[client] == -1)
	{
		ResetPartyIcon(client);
		return -1;
	}
	
	if(g_aPartyMembers[g_iParty[client]] == null)
	{
		g_iParty[client] = -1;
		ResetPartyIcon(client);
		return -1;
	}
	
	int members = GetArraySize(g_aPartyMembers[g_iParty[client]]);
	
	for (int i = 0; i < members; i++)
	{
		if(GetArrayCell(g_aPartyMembers[g_iParty[client]], i) != client)
			continue;
		
		return i;
	}
	
	g_iParty[client] = -1;
	ResetPartyIcon(client);
	
	return -1;
}

public int Native_GetDistrictLimit(Handle plugin, int numParams)
{
	return g_iPartyLimit;
}
