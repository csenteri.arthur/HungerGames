float g_fNextEventTime;
Handle g_hEventTimer = null;

#include "hungergames/events/ioncannon.sp"
#include "hungergames/events/napalm.sp"
#include "hungergames/events/thunderstorm.sp"
#include "hungergames/events/chargedwater.sp"
#include "hungergames/events/earthquake.sp"
#include "hungergames/events/artillerie.sp"

enum
{
	EV_None = 0,
	EV_IonCannon,
	EV_Napalm,
	EV_ThunderStorm,
	EV_ChargedWater,
	EV_Earthquake,
	EV_Artillery
};

// Contains the translation key
char g_sEvents[][] = {
	"Event_None",
	"Event_IonCannon",
	"Event_Napalm",
	"Event_ThunderStorm",
	"Event_ChargedWater",
	"Event_Earthquake",
	"Event_Artillery"
};

Handle g_hEvents = null;
Handle g_hEventsPool = null;

bool g_bVoting;
int g_iVoted[MAXPLAYERS + 1];
int g_iVoteOptions[2];

void RegisterEvents()
{
	if(g_hEvents != null)
		ClearArray(g_hEvents);
	else g_hEvents = CreateArray(1);
	
	if(g_hEventsPool != null)
		ClearArray(g_hEventsPool);
	else g_hEventsPool = CreateArray(1);
	
	if(g_iEventIonCannon > 0)
	{
		PushArrayCell(g_hEventsPool, EV_IonCannon);
		for (int x = 0; x < g_iEventIonCannon; x++)
			PushArrayCell(g_hEvents, EV_IonCannon);
	}
	
	if(g_iEventNapalm > 0)
	{
		PushArrayCell(g_hEventsPool, EV_Napalm);
		for (int x = 0; x < g_iEventNapalm; x++)
			PushArrayCell(g_hEvents, EV_Napalm);
	}
	
	if(g_iEventThunderStorm > 0)
	{
		PushArrayCell(g_hEventsPool, EV_ThunderStorm);
		for (int x = 0; x < g_iEventThunderStorm; x++)
			PushArrayCell(g_hEvents, EV_ThunderStorm);
	}
	
	if(g_iEventChargedWater > 0)
	{
		PushArrayCell(g_hEventsPool, EV_ChargedWater);
		for (int x = 0; x < g_iEventChargedWater; x++)
			PushArrayCell(g_hEvents, EV_ChargedWater);
	
	}
	
	if(g_iEventEarthquake > 0)
	{
		PushArrayCell(g_hEventsPool, EV_Earthquake);
		for (int x = 0; x < g_iEventEarthquake; x++)
			PushArrayCell(g_hEvents, EV_Earthquake);
	}
	
	if(g_iEventArtilerie > 0)
	{
		PushArrayCell(g_hEventsPool, EV_Artillery);
		for (int x = 0; x < g_iEventArtilerie; x++)
			PushArrayCell(g_hEvents, EV_Artillery);
	}
}

/* Event Timer */

void StartEventTimer()
{
	if(g_hEventTimer != null)
	{
		CloseHandle(g_hEventTimer);
		g_hEventTimer = null;
	}
	
	g_hEventTimer = CreateTimer(1.0, Timer_EventManager, _, TIMER_REPEAT);
}

public Action Timer_EventManager(Handle timer, any data)
{
	// Idle
	if(g_fNextEventTime == 0.0)
		return Plugin_Continue;
	
	int iAlive;
	LoopAlivePlayers(i)
	{
		if(!g_bIsTribute[i])
			continue;
			
		iAlive++;
	}
	
	// No Players
	if(iAlive < 1)
		return Plugin_Continue;
		
	if(RoundStatus != RS_InProgress && !(g_bEventEndgame && RoundStatus == RS_Endgame))
	{
		ResetNextEvent();
		ResetEventVoting();
		
		return Plugin_Continue;
	}
	
	// Start event with most voting or just the first random event
	if(GetGameTime() > g_fNextEventTime)
	{
		if(g_bVoting)
		{
			g_bVoting = false;
			EndEventVoting();
		}
		else StartEvent(true);
	}
	else
	{
		// Start event voting
		if(!g_bVoting && g_fEventVoteTime > 0.0 && GetGameTime() > g_fNextEventTime - g_fEventVoteTime && GetArraySize(g_hEvents) > 1)
		{
			g_bVoting = true;
			StartEventVoting();
		}
	}
	
	return Plugin_Continue;
}

void ResetNextEvent()
{
	g_fNextEventTime = 0.0;
}

void SetNextEvent(bool first, float eventtime = 0.0)
{
	if (g_fEventDelay <= 0.0)
		return;
	
	if(first)
		g_fNextEventTime = GetGameTime() + g_fEventDelay;
	else g_fNextEventTime = GetGameTime() + GetRandomFloat(g_fEventIntervalMin, g_fEventIntervalMax) + eventtime;
}

/* Voting */

void SetVoteResults()
{
	g_iVoteOptions[0] = GetArrayCell(g_hEventsPool, GetRandomInt(0, GetArraySize(g_hEventsPool)-1));
	g_iVoteOptions[1] = EV_None;
	
	while(g_iVoteOptions[1] == EV_None || g_iVoteOptions[0] == g_iVoteOptions[1])
	{
		g_iVoteOptions[1] = GetArrayCell(g_hEventsPool, GetRandomInt(0, GetArraySize(g_hEventsPool)-1));
	}
}

int GetRandomEvent()
{
	int iSize = GetArraySize(g_hEventsPool);
	if(iSize < 1)
		return EV_None;
	
	return GetArrayCell(g_hEventsPool, GetRandomInt(0, iSize-1));
}

void ResetEventVoting()
{
	LoopClients(i)
		g_iVoted[i] = -1;
}

void StartEventVoting()
{
	SetVoteResults();
	
	LoopIngameClients(client)
	{
		if(g_bIsTribute[client])
			continue;
		
		Menu_EventVoting(client)
	}
}

void EndEventVoting()
{
	StartEvent(false);
}

/* Event manager */

void StartEvent(bool random)
{
	int event = EV_None;
	
	if(random)
		event = GetRandomEvent();
	else
	{
		int iVoteCount[2];
		
		LoopClients(i)
		{
			if(g_iVoted[i] == -1)
				continue;
			
			iVoteCount[g_iVoted[i]]++;
		}
		
		if(iVoteCount[0] != iVoteCount[1])
			event = (iVoteCount[0] > iVoteCount[1]) ? g_iVoteOptions[0] : g_iVoteOptions[1];
		else event = g_iVoteOptions[GetRandomInt(0, 1)];
	}
	
	StartNewEvent(event, 0);
}

void StartNewEvent(int event, int target)
{
	// Get a random target if no target is selected
	if(target == 0)
		target = Client_GetRandom(CLIENTFILTER_INGAME | CLIENTFILTER_ALIVE);
	
	float fEventtime;
	
	if(event > EV_None)
	{
		LoopIngamePlayers(i)
		{
			char sEvent[64];
			Format(sEvent, sizeof(sEvent), "%T", g_sEvents[event], i);
			CPrintToChat(i, "%T", "Event_Activated", i, g_sPrefix, sEvent);
		}
	}
	
	if(event == EV_IonCannon)
	{
		StartIonCannon(target);
		fEventtime = 20.0;
	}
	else if(event == EV_Napalm)
	{
		StartNapalmAirStrike(target);
		fEventtime = 0.3*float(g_iEventNapalmProjectiles);
	}
	else if(event == EV_ThunderStorm)
	{
		StartThunderStorm(target);
		fEventtime = g_fEventThunderStormInterval * float(g_iEventThunderStormLightnings);
	}
	else if(event == EV_ChargedWater)
	{
		StartChargedWater();
		fEventtime = g_fEventChargedWaterInterval*float(g_iEventChargedWaterSimulationsMax);
	}
	else if(event == EV_Earthquake)
	{
		StartEarthquake();
		fEventtime = g_fEventEarthquakeDuration;
	}
	else if(event == EV_Artillery)
	{
		StartArtillerieAirStrike(target);
		fEventtime = 0.3*float(g_iEventArtillerieProjectiles);
	}
	
	SetNextEvent(false, fEventtime);
}

public Action Cmd_Event(int client, int args)
{
	char sBuffer[32];
	GetCmdArg(1, sBuffer, sizeof(sBuffer));
	
	StartNewEvent(StringToInt(sBuffer), 0);
	
	return Plugin_Handled;
}

/* Vote Menu */

void Menu_EventVoting(int client)
{
	Menu menu = new Menu(Menu_EventVoting_Handler);
	
	menu.SetTitle("%T", "Event_VoteNow", client);
	
	char sBuffer[64];
	Format(sBuffer, sizeof(sBuffer), "%T", g_sEvents[0], client);
	menu.AddItem("0", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", g_sEvents[g_iVoteOptions[0]], client);
	menu.AddItem("1", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", g_sEvents[g_iVoteOptions[1]], client);
	menu.AddItem("2", sBuffer);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(client, RoundToFloor(g_fEventVoteTime));
}

public int Menu_EventVoting_Handler(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		if(g_bVoting && !g_bIsTribute[client] && RoundStatus == RS_InProgress || RoundStatus == RS_Endgame)
		{
			char sInfo[16];
			GetMenuItem(menu, params, sInfo, sizeof(sInfo));
			int option = StringToInt(sInfo)-1;
			
			g_iVoted[client] = option;
			
			if(option == -1)
			{
				CPrintToChat(client, "%T", "Event_VotedNothing", client, g_sPrefix);
			}
			else 
			{
				char sEvent[64];
				Format(sEvent, sizeof(sEvent), "%T", g_sEvents[g_iVoteOptions[option]], client);
				CPrintToChat(client, "%T", "Event_Voted", client, g_sPrefix, sEvent);
			}
		}
		else 
		{
			CPrintToChat(client, "%T", "Event_VoteNotAllowed", client, g_sPrefix);
		}
	}
	else if (action == MenuAction_End)
	{
		CloseHandle(menu);
	}
}

public void OnEntityCreated(int iEntity, const char[] classname)
{
	if(StrEqual(classname, "molotov_projectile"))
		SDKHook(iEntity, SDKHook_Spawn, OnMolotovProjectile);
	else if(StrEqual(classname, "hegrenade_projectile"))
		SDKHook(iEntity, SDKHook_Spawn, OnHegrenadeProjectile);
}