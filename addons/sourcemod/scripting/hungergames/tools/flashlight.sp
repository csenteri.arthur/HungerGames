public Action Command_FlashlightToogle(int client, const char[] command, int argc)
{
	if(!client || !IsClientInGame(client) || !IsPlayerAlive(client))
		return Plugin_Continue;
		
	if(!g_bIsTribute[client])
		return Plugin_Continue;
	
	ToggleFlashlight(client);
		
	return Plugin_Continue;
}

void ToggleFlashlight(int client)
{
	SetEntProp(client, Prop_Send, "m_fEffects", GetEntProp(client, Prop_Send, "m_fEffects") ^ 4); 
}