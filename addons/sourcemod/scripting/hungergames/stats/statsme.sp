void Cmd_Stats(int client)
{
	if(!g_bConnected || !g_pZcoreMysql)
	{
		CPrintToChat(client, "%T", "NoDatabaseConnection", client, g_sPrefix);
		return;
	}
	
	if(g_bStatsMeShowAll)
	{
		LoopIngamePlayers(i)
		{
			char sName[32];
			GetClientName(i, sName, sizeof(sName));
			
			CPrintToChat(i, "%T", "StatsmeAll", i, g_sPrefix, sName, g_iWinRank[client], g_iEloRank[client], g_iPointsRank[client], g_iPoints[client], g_iWins[client], g_iKills[client], g_iKnifeKills[client], g_iDeaths[client], g_iElo[client], g_iEloMin[client], g_iEloMax[client], g_iTotalPlayers, g_iAnnual);
			
			if(g_iStatsMeMessages > 1)
				CPrintToChat(i, "%T", "StatsmeAll2", i, g_sPrefix, sName, g_iWinRank[client], g_iEloRank[client], g_iPointsRank[client], g_iPoints[client], g_iWins[client], g_iKills[client], g_iKnifeKills[client], g_iDeaths[client], g_iElo[client], g_iEloMin[client], g_iEloMax[client], g_iTotalPlayers, g_iAnnual);
			
			if(g_iStatsMeMessages > 2)
				CPrintToChat(i, "%T", "StatsmeAll3", i, g_sPrefix, sName, g_iWinRank[client], g_iEloRank[client], g_iPointsRank[client], g_iPoints[client], g_iWins[client], g_iKills[client], g_iKnifeKills[client], g_iDeaths[client], g_iElo[client], g_iEloMin[client], g_iEloMax[client], g_iTotalPlayers, g_iAnnual);
		}
	}
	else
	{
		CPrintToChat(client, "%T", "Statsme", client, g_sPrefix, g_iWinRank[client], g_iEloRank[client], g_iPointsRank[client], g_iPoints[client], g_iWins[client], g_iKills[client], g_iKnifeKills[client], g_iDeaths[client], g_iElo[client], g_iEloMin[client], g_iEloMax[client], g_iTotalPlayers, g_iAnnual);
		
		if(g_iStatsMeMessages > 1)
			CPrintToChat(client, "%T", "Statsme2", client, g_sPrefix, g_iWinRank[client], g_iEloRank[client], g_iPointsRank[client], g_iPoints[client], g_iWins[client], g_iKills[client], g_iKnifeKills[client], g_iDeaths[client], g_iElo[client], g_iEloMin[client], g_iEloMax[client], g_iTotalPlayers, g_iAnnual);
		
		if(g_iStatsMeMessages > 2)
			CPrintToChat(client, "%T", "Statsme3", client, g_sPrefix, g_iWinRank[client], g_iEloRank[client], g_iPointsRank[client], g_iPoints[client], g_iWins[client], g_iKills[client], g_iKnifeKills[client], g_iDeaths[client], g_iElo[client], g_iEloMin[client], g_iEloMax[client], g_iTotalPlayers, g_iAnnual);
	}
}