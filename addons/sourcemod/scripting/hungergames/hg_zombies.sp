bool g_bIsZombie[MAXPLAYERS + 1];

Handle g_hZombieActivateTimer = null;
Handle g_hZombieWaveTimer = null;

float g_fNextHorde;

bool IsZombie(int client)
{
	return g_bIsZombie[client];
}

void ResetZombie(int client)
{
	g_bIsZombie[client] = false;
}

void ResetZombieTimers(bool resetPlayers = true)
{
	if(resetPlayers)
	{
		LoopClients(client)
			ResetZombie(client);
	}
	
	if(g_hZombieActivateTimer != null)
	{
		CloseHandle(g_hZombieActivateTimer);
		g_hZombieActivateTimer = null;
	}
	
	if(g_hZombieWaveTimer != null)
	{
		CloseHandle(g_hZombieWaveTimer);
		g_hZombieWaveTimer = null;
	}
	
	g_fNextHorde = 0.0;
}

void StartZombieActivateTimer()
{
	if(g_hZombieActivateTimer == null && g_hZombieWaveTimer == null && g_fZombieActivateDelay >= 0.0)
		g_hZombieActivateTimer = CreateTimer(g_fZombieActivateDelay, Timer_ZombieActivate);
}

public Action Timer_ZombieActivate(Handle timer, any data)
{
	// Round still in progress, lets announce the first wave incoming
	if(RoundStatus == RS_InProgress)
	{
		LoopIngameClients(i)
		{
			CPrintToChat(i, "%T", "Zombie_FirstWave", i, g_sPrefix, RoundToFloor(g_fZombieWaveInterval));
		}
		
		StartZombieWaveTimer();
	}
	
	g_hZombieActivateTimer = null;
	
	return Plugin_Handled;
}

int g_iWaveCountdown;

void StartZombieWaveTimer()
{
	if(g_hZombieWaveTimer != null)
	{
		CloseHandle(g_hZombieWaveTimer);
		g_hZombieWaveTimer = null;
	}
	
	g_iWaveCountdown = -1;
	g_hZombieWaveTimer = CreateTimer(1.0, Timer_ZombieWaveCountdown, _, TIMER_REPEAT);
	g_fNextHorde = GetGameTime()+g_fZombieWaveInterval;
}

public Action Timer_ZombieWaveCountdown(Handle timer, any data)
{
	if(g_fNextHorde <= 0.0)
	{
		g_hZombieWaveTimer = null;
		g_fNextHorde = 0.0;
		return Plugin_Stop;
	}
	
	if(RoundStatus != RS_InProgress)
	{
		g_hZombieWaveTimer = null;
		g_fNextHorde = 0.0;
		return Plugin_Stop;
	}
	
	if(g_iWaveCountdown > 0)
		g_iWaveCountdown--;
	
	// Start countdown
	float timeLeft = g_fNextHorde - GetGameTime();
	if (timeLeft <= 10 && g_iWaveCountdown == -1)
		g_iWaveCountdown = 10;
	
	// Spawn wave
	if(g_iWaveCountdown == 0)
	{
		LoopIngameClients(i)
		{
			CPrintToChat(i, "%T", "Zombie_NextWave", i, g_sPrefix, RoundToFloor(g_fZombieWaveInterval));
		}
		
		SpawnWave();
		g_fNextHorde = GetGameTime() + g_fZombieWaveInterval;
		g_iWaveCountdown = -1;
		return Plugin_Continue;
	}
	
	// Countdown
	if(g_iWaveCountdown > 0)
	{
		LoopIngameClients(client)
		{	
			if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
				continue;
			
			if(IsPlayerAlive(client))
				continue;
			
			CPrintToChat(client, "%T", "Zombie_Countdown", client, g_sPrefix, g_iWaveCountdown);
			
			EmitSoundToClientAny(client, sndCountdown[g_iWaveCountdown-1]);
		}
	}
	
	return Plugin_Continue;
}

void SpawnWave()
{
	int zCount;
	
	LoopIngameClients(client)
	{	
		if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
			continue;
			
		if(IsPlayerAlive(client))
		{
			// Heal "alive" zombies
			if(g_bIsZombie[client] && g_iZombieHeal > 0)
			{
				if(GetClientHealth(client) < g_iZombieHeal)
					Entity_SetHealth(client, g_iZombieHeal);
			}
			
			continue;
		}
		
		g_bIsZombie[client] = true;
		CS_RespawnPlayer(client);
		zCount++;
	}
	
	if(zCount > 0 && g_bSndWave)
	{
		float pos[3];
		EmitAmbientSoundAny(sndWave, pos);
	}
}

public Action Timer_ApplyZombie(Handle timer, any userid)
{
	int client = GetClientOfUserId(userid);
	if(client && IsClientInGame(client) && IsPlayerAlive(client) && IsZombie(client))
		ApplyZombie(client, true);
	
	return Plugin_Handled;
}

void ApplyZombie(int client, bool delayed = false)
{
	SetEntityModel(client, g_sZombieModel);
	SetArmsModel(client);
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", g_fZombieSpeedDefault);
	SetEntProp(client, Prop_Send, "m_iFOV", g_iZombieFOV);
	SetEntProp(client, Prop_Send, "m_ArmorValue", g_iZombieArmor);
	SetClientGravity(client, g_fZombieGravity);
	ShowRadar(client);
	
	if(g_bZombieThirdperson)
		SetThirdPersonView(client, true);
	
	if(g_iZombieHealth > 0)
		SetEntityHealth(client, g_iZombieHealth);
	
	if(delayed)
	{
		StripPlayerWeapons(client, true, true);
		GiveZombieKnife(client);
	}
}

void GiveZombieKnife(int client)
{
	if(!IsClientInGame(client))
		return;
		
	if(!IsPlayerAlive(client))
		return;
		
	if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
		return;
		
	if(!g_bIsZombie[client])
		return;
		
	EquipPlayerWeapon(client, GivePlayerItem(client, "weapon_knife"));
}

void StartZombieSpeedTimer()
{
	CreateTimer(1.0, Timer_ZombieSpeed, _, TIMER_REPEAT);
}

public Action Timer_ZombieSpeed(Handle timer, any data)
{
	if(g_fZombieSpeedDistance <= 0.0)
		return Plugin_Continue;
		
	if(RoundStatus < RS_InProgress)
		return Plugin_Continue;
	
	LoopIngamePlayers(client)
	{
		if(!g_bIsZombie[client])
			continue;
			
		if(!IsPlayerAlive(client))
			continue;
		
		UpdateZombieSpeed(client);
	}
	
	return Plugin_Continue;
}

void UpdateZombieSpeed(int client)
{
	float dist = GetClosestTributeDistance(client);
	
	float speed = g_fZombieSpeedDefault;
	
	if(dist != -1.0 && dist < g_fZombieSpeedDistance)
		speed = g_fZombieSpeedNear;
	
	SetEntPropFloat(client, Prop_Data, "m_flLaggedMovementValue", speed);
}

float GetClosestTributeDistance(int client)
{
	float fPos[3];
	GetClientAbsOrigin(client, fPos);
	
	float fDistance;
	float fClosestDistance = -1.0;
	
	LoopAlivePlayers(i)
	{
		if(i == client)
			continue;
			
		if(!g_bIsTribute[i])
			continue;
		
		float fTargetPos[3];
		GetClientAbsOrigin(i, fTargetPos);
		
		if(fTargetPos[0] == 0.0 && fTargetPos[1] == 0.0 && fTargetPos[2] == 0.0)
			continue;
		
		fDistance = GetVectorDistance(fPos, fTargetPos);
		
		if (fDistance < fClosestDistance || fClosestDistance == -1.0)
			fClosestDistance = fDistance;
	}
	
	return fClosestDistance;
}

char g_sOldArmsModel[MAXPLAYERS + 1][PLATFORM_MAX_PATH];

void SetArmsModel(int client)
{
	if(strlen(g_sZombieArmsModel) < 1)
		return;
	
	if(!FileExists(g_sZombieArmsModel) && !FileExists(g_sZombieArmsModel, true))
		return;
	
	char sBuffer[PLATFORM_MAX_PATH];
	GetEntPropString(client, Prop_Send, "m_szArmsModel", sBuffer, sizeof(sBuffer));
	
	if (!StrEqual(sBuffer, g_sZombieArmsModel))
	{
		strcopy(g_sOldArmsModel[client], sizeof(g_sOldArmsModel[]), sBuffer);
		SetEntPropString(client, Prop_Send, "m_szArmsModel", g_sZombieArmsModel);
	}
}

void ResetArmsModel(int client)
{
	if(strlen(g_sZombieArmsModel) < 1)
		return;
	
	if(!FileExists(g_sZombieArmsModel) && !FileExists(g_sZombieArmsModel, true))
		return;
	
	char sBuffer[PLATFORM_MAX_PATH];
	GetEntPropString(client, Prop_Send, "m_szArmsModel", sBuffer, sizeof(sBuffer));
	
	if (StrEqual(sBuffer, g_sZombieArmsModel))
	{
		SetEntPropString(client, Prop_Send, "m_szArmsModel", g_sOldArmsModel[client]);
	}
}