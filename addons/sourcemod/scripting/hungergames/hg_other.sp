/* Macros */

#define LoopClients(%1) for(int %1 = 1; %1 <= MaxClients; %1++)

#define LoopIngameClients(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1))

#define LoopIngamePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && !IsFakeClient(%1))

#define LoopAlivePlayers(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1))

#define LoopTributes(%1) for(int %1=1;%1<=MaxClients;++%1)\
if(IsClientInGame(%1) && IsPlayerAlive(%1) && g_bIsTribute[%1])

/* Spec Modes */

#define SPECMODE_NONE 					0
#define SPECMODE_FIRSTPERSON 			4
#define SPECMODE_3RDPERSON 				5
#define SPECMODE_FREELOOK	 			6


void RemoveHudDelayed(int client)
{
	CreateTimer(0.0, Timer_HideHUD, GetClientUserId(client));
}

public Action Timer_HideHUD(Handle timer, any userid)
{
	int client = GetClientOfUserId(userid);
	if(client && IsClientInGame(client) && IsPlayerAlive(client))
		HideRadar(client);
}

/* Damage Types */

#define DMG_HEADSHOT			(1 << 30)

/* Hide HUD Flags */

#define HIDEHUD_CSGO_RADAR		( 1<<12 )
#define HUD_CSGO_RADAR			( 0<<12 )

void HideRadar(int client)
{
	if(IsClientConnected(client) && !IsFakeClient(client))
		SetEntProp(client, Prop_Send, "m_iHideHUD", HIDEHUD_CSGO_RADAR);
}

void ShowRadar(int client)
{
	if(IsClientConnected(client) && !IsFakeClient(client))
		SetEntProp(client, Prop_Send, "m_iHideHUD", HUD_CSGO_RADAR);
}

/* Weapons */

void StripPlayerWeapons(int client, bool remove, bool keepknife = false)
{
	int iWeapon = -1;
	for(int i = CS_SLOT_PRIMARY;i<=CS_SLOT_C4;i++)
	{
		if(keepknife && i == CS_SLOT_KNIFE)
			continue;
		
		while((iWeapon = GetPlayerWeaponSlot(client, i)) != -1)
		{
			if(remove)
				RemovePlayerItem(client, iWeapon);
			else CS_DropWeapon(client, iWeapon, false);
		}
	}
}

void BlockWeapon(int client, int weapon, float time)
{
	float fUnlockTime = GetGameTime() + time;
	
	SetEntPropFloat(client, Prop_Send, "m_flNextAttack", fUnlockTime);
	
	if(weapon > 0)
		SetEntPropFloat(weapon, Prop_Send, "m_flNextPrimaryAttack", fUnlockTime);
}

/* CSGO Weapon Types */

#define WEAPONTYPE_UNKNOWN -1
#define WEAPONTYPE_KNIFE 0
#define WEAPONTYPE_PISTOL 1
#define WEAPONTYPE_SUBMACHINEGUN 2
#define WEAPONTYPE_RIFLE 3
#define WEAPONTYPE_SHOTGUN 4
#define WEAPONTYPE_SNIPER_RIFLE 5
#define WEAPONTYPE_MACHINEGUN 6
#define WEAPONTYPE_C4 7
#define WEAPONTYPE_TASER 8 
#define WEAPONTYPE_GRENADE 9 
#define WEAPONTYPE_HEALTHSHOT 11

stock int GetSlotsByWeaponType(int type)
{
	switch(type)
	{
		case WEAPONTYPE_KNIFE:
		{
			return CS_SLOT_KNIFE+1;
		}
		case WEAPONTYPE_TASER:
		{
			return CS_SLOT_KNIFE+1;
		}
		case WEAPONTYPE_PISTOL:
		{
			return CS_SLOT_SECONDARY+1;
		}
		case WEAPONTYPE_SUBMACHINEGUN:
		{
			return CS_SLOT_PRIMARY+1;
		}
		case WEAPONTYPE_RIFLE:
		{
			return CS_SLOT_PRIMARY+1;
		}
		case WEAPONTYPE_SHOTGUN:
		{
			return CS_SLOT_PRIMARY+1;
		}
		case WEAPONTYPE_SNIPER_RIFLE:
		{
			return CS_SLOT_PRIMARY+1;
		}
		case WEAPONTYPE_MACHINEGUN:
		{
			return CS_SLOT_PRIMARY+1;
		}
		case WEAPONTYPE_C4:
		{
			return CS_SLOT_C4+1;
		}
		case WEAPONTYPE_HEALTHSHOT:
		{
			return CS_SLOT_C4+1;
		}
		case WEAPONTYPE_GRENADE:
		{
			return CS_SLOT_GRENADE+1;
		}
	}
	
	return 0;
}

int g_iSwitchToSlot[MAXPLAYERS + 1];

stock int SwitchToSlotDelayed(int client, int slot, float delay)
{
	if(delay < 0.0)
		SwitchToSlot(client, slot);
	else
	{
		g_iSwitchToSlot[client] = slot;
		CreateTimer(delay, Timer_SwitchWeapon, client, TIMER_FLAG_NO_MAPCHANGE);
	}
}

public Action Timer_SwitchWeapon(Handle timer, any client)
{
	if(IsClientInGame(client))
		SwitchToSlot(client, g_iSwitchToSlot[client]);
}

stock int SwitchToSlot(int client, int slot)
{
	ClientCommand(client, "slot%d", slot);
}

void StartCleanupTimer()
{
	if(g_fWeaponCleanerInterval > 0.0)
		CreateTimer(g_fWeaponCleanerInterval, Timer_CleanupWeapons, _, TIMER_REPEAT);
}

public Action Timer_CleanupWeapons(Handle timer, any data)
{
	CleanupWeapons(g_fWeaponCleanerDistance);
	
	return Plugin_Continue;
}

void CleanupWeapons(float distance = -1.0)
{
	if(g_fWeaponCleanerInterval <= 0.0)
		return;
	
	static int g_oWeaponParent;
	
	if(g_oWeaponParent <= 0)
		g_oWeaponParent = FindSendPropInfo("CBaseCombatWeapon", "m_hOwnerEntity");
	
	int maxent = GetMaxEntities();
	char sWeapon[64];
	for (int i=GetMaxClients(); i < maxent; i++)
	{
		if (!IsValidEdict(i) || !IsValidEntity(i))
			continue;
		
		GetEdictClassname(i, sWeapon, sizeof(sWeapon));
		if (!((StrContains(sWeapon, "weapon_") != -1 || StrContains(sWeapon, "item_") != -1 )))
			continue;
			
		if (GetEntDataEnt2(i, g_oWeaponParent) != -1)
			continue;
		
		if(distance != -1.0)
		{	
			float fPos[3];
			Entity_GetAbsOrigin(i, fPos);
			
			float dist = GetClosestPlayerDistance(fPos);
				
			if(dist == -1.0 || dist > distance)
				RemoveEdict(i);
		}
		else RemoveEdict(i);
	}
}

float GetClosestPlayerDistance(float pos[3])
{
	float fDistance;
	float fClosestDistance = -1.0;
	
	LoopAlivePlayers(i)
	{
		float fTargetPos[3];
		GetClientAbsOrigin(i, fTargetPos);
		
		if(fTargetPos[0] == 0.0 && fTargetPos[1] == 0.0 && fTargetPos[2] == 0.0)
			continue;
		
		fDistance = GetVectorDistance(pos, fTargetPos);
		
		if (fDistance < fClosestDistance || fClosestDistance == -1.0)
			fClosestDistance = fDistance;
	}
	
	return fClosestDistance;
}

void GetCircuitPos(float center[3], float radius, float angle, float output[3], bool rotate = false, bool horizontal = false)
{
	float sin=Sine(DegToRad(angle))*radius;
	float cos=Cosine(DegToRad(angle))*radius;
	
	if(horizontal)
	{
		output[0] = center[0]+sin;
		output[1] = center[1]+cos;
		output[2] = center[2];
	}
	else
	{
		if(rotate)
		{
			output[0] = center[0]+sin;
			output[1] = center[1];
			output[2] = center[2]+cos;
		}
		else{
			output[0] = center[0];
			output[1] = center[1]+sin;
			output[2] = center[2]+cos;
		}
	}
}

stock int GetClientAimTargetPos(int client, float pos[3]) 
{
	if(client < 1) 
		return -1;
	
	float vAngles[3];
	float vOrigin[3];
	
	GetClientEyePosition(client,vOrigin);
	GetClientEyeAngles(client, vAngles);
	
	Handle trace = TR_TraceRayFilterEx(vOrigin, vAngles, MASK_SHOT, RayType_Infinite, TraceFilterAllEntities, client);
	
	TR_GetEndPosition(pos, trace);
	int entity = TR_GetEntityIndex(trace);
	
	CloseHandle(trace);
	
	return entity;
}

public bool TraceFilterAllEntities(int entity, int contentsMask, any client)
{
	if(entity == client)
		return false;
	
	if(entity > MaxClients)
		return false;
	
	return true;
}

stock void LookAtPoint(int client, float point[3])
{
	float angles[3]; float clientEyes[3]; float resultant[3];
	GetClientEyePosition(client, clientEyes);
	MakeVectorFromPoints(point, clientEyes, resultant);
	GetVectorAngles(resultant, angles);
	
	if (angles[0] >= 270)
	{
		angles[0] -= 270;
		angles[0] = (90 - angles[0]);
	}
	else if (angles[0] <= 90)
		angles[0] *= -1;
	
	angles[1] -= 180;
	TeleportEntity(client, NULL_VECTOR, angles, NULL_VECTOR);
}

bool AddSeperator(int &iEqLineCount, char centerText[512])
{
	if(iEqLineCount >= 2)
	{
		Format(centerText, sizeof(centerText), "%s\n", centerText);
		iEqLineCount = 0;
		return true;
	}
	
	if(iEqLineCount > 0)
		Format(centerText, sizeof(centerText), "%s, ", centerText);
	
	return false;
}

void Point_Hurt(int nClientVictim, int nDamage, int nClientAttacker = 0, int nDamageType = DMG_GENERIC, char[] sWeapon = "")
{
	if(	nClientVictim > 0 &&
			IsValidEntity(nClientVictim) &&
			IsClientInGame(nClientVictim) &&
			IsPlayerAlive(nClientVictim) &&
			nDamage > 0)
	{
		int EntityPointHurt = CreateEntityByName("point_hurt");
		if(EntityPointHurt != 0)
		{
			char sDamage[16];
			IntToString(nDamage, sDamage, sizeof(sDamage));

			char sDamageType[32];
			IntToString(nDamageType, sDamageType, sizeof(sDamageType));

			DispatchKeyValue(nClientVictim,			"targetname",		"war3_hurtme");
			DispatchKeyValue(EntityPointHurt,		"DamageTarget",		"war3_hurtme");
			DispatchKeyValue(EntityPointHurt,		"Damage",			sDamage);
			DispatchKeyValue(EntityPointHurt,		"DamageType",		sDamageType);
			
			if(!StrEqual(sWeapon, ""))
				DispatchKeyValue(EntityPointHurt,	"classname",		sWeapon);
				
			DispatchSpawn(EntityPointHurt);
			AcceptEntityInput(EntityPointHurt,		"Hurt",				(nClientAttacker != 0) ? nClientAttacker : -1);
			DispatchKeyValue(EntityPointHurt,		"classname",		"point_hurt");
			DispatchKeyValue(nClientVictim,			"targetname",		"war3_donthurtme");

			RemoveEdict(EntityPointHurt);
		}
	}
}

stock void Env_Shake(float Origin[3], float Amplitude, float Radius, float Duration, float Frequency)
{
	int Ent;
	
	Ent = CreateEntityByName("env_shake");
	
	if(DispatchSpawn(Ent))
	{
		DispatchKeyValueFloat(Ent, "amplitude", Amplitude);
		DispatchKeyValueFloat(Ent, "radius", Radius);
		DispatchKeyValueFloat(Ent, "duration", Duration);
		DispatchKeyValueFloat(Ent, "frequency", Frequency);

		SetVariantString("spawnflags 8");
		AcceptEntityInput(Ent,"AddOutput");
		
		AcceptEntityInput(Ent, "StartShake", 0);
		
		TeleportEntity(Ent, Origin, NULL_VECTOR, NULL_VECTOR);
		
		RemoveEntity(Ent, 30.0);
	}
}

stock void MakeShakeScreen(int client, float intensity, float frequency, float duration)
{
	Handle hPB = StartMessageOne("Shake", client);
	if(hPB != null)
	{
		PbSetInt(hPB, "command", 0);
		PbSetFloat(hPB, "local_amplitude", intensity);
		PbSetFloat(hPB, "frequency", frequency);
		PbSetFloat(hPB, "duration", duration);
		EndMessage();
	}
}

stock bool Env_Explosion(int attacker = 0, int inflictor = -1, const float attackposition[3], const char[] weaponname = "", int magnitude = 100, int radiusoverride = 0, float damageforce = 0.0, int flags = 0)
{
	
	int explosion = CreateEntityByName("env_explosion");
	
	if(explosion != -1)
	{
		DispatchKeyValueVector(explosion, "Origin", attackposition);
		
		char intbuffer[64];
		IntToString(magnitude, intbuffer, 64);
		DispatchKeyValue(explosion,"iMagnitude", intbuffer);
		if(radiusoverride > 0)
		{
			IntToString(radiusoverride, intbuffer, 64);
			DispatchKeyValue(explosion,"iRadiusOverride", intbuffer);
		}
		
		if(damageforce > 0.0)
			DispatchKeyValueFloat(explosion,"DamageForce", damageforce);

		if(flags != 0)
		{
			IntToString(flags, intbuffer, 64);
			DispatchKeyValue(explosion,"spawnflags", intbuffer);
		}

		if(!StrEqual(weaponname, "", false))
			DispatchKeyValue(explosion,"classname", weaponname);

		DispatchSpawn(explosion);
		if(attacker > 0 && Client_IsValid(attacker, true) &&  IsClientInGame(attacker))
			SetEntPropEnt(explosion, Prop_Send, "m_hOwnerEntity", attacker);

		if(inflictor != -1)
			SetEntPropEnt(explosion, Prop_Data, "m_hInflictor", inflictor);
			
		AcceptEntityInput(explosion, "Explode");
		AcceptEntityInput(explosion, "Kill");
		
		return (true);
	}
	else
		return (false);
}

float AngleDown[3] = { 90.0, 0.0, 0.0 };
float AngleUp[3] = { -90.0, 0.0, 0.0 };

#define	MASK_AIRDROP (CONTENTS_SOLID|MASK_OPAQUE|CONTENTS_IGNORE_NODRAW_OPAQUE|CONTENTS_MOVEABLE|CONTENTS_WINDOW|CONTENTS_PLAYERCLIP|CONTENTS_GRATE)

stock GetRoofDistance(int client, float &distance)
{
	float pos[3]; 
	
	GetClientAbsOrigin(client, pos);
	
	pos[2] += 32.0;

	Handle trace = TR_TraceRayFilterEx(pos, AngleUp, MASK_AIRDROP, RayType_Infinite, TraceRay_DontHitPlayers);
	
	if (TR_DidHit(trace)) 
	{
		float posEnd[3];
		TR_GetEndPosition(posEnd, trace);
		distance = GetVectorDistance(pos, posEnd);
	}
}

public bool TraceRay_DontHitPlayers(int entity, int mask, any data)
{
	if(0 < entity <= MaxClients)
		return false;
	return true;
}

public Action MsgHook_AdjustMoney(UserMsg msg_id, Handle msg, const int[] players, int playersNum, bool reliable, bool init)
{
	char buffer[64];
	PbReadString(msg, "params", buffer, sizeof(buffer), 0);
	
	if (StrContains(buffer, "Cash_Award") != -1)
		return Plugin_Handled;
	
	if (StrContains(buffer, "Point_Award") != -1)
		return Plugin_Handled;
		
	return Plugin_Continue;
}

// Set third/firstperson view

bool SetThirdPersonView(int client, bool third)
{
	static Handle m_hAllowTP = INVALID_HANDLE;
	if(m_hAllowTP == INVALID_HANDLE)
		m_hAllowTP = FindConVar("sv_allow_thirdperson");

	SetConVarInt(m_hAllowTP, 1);

	if(third)
		ClientCommand(client, "thirdperson");
	else ClientCommand(client, "firstperson");
	return false;
}

stock void GetWorldMax(float fMin[3], float fMax[3], float fTop[3])
{
	GetEntPropVector(0, Prop_Send, "m_WorldMaxs", fMax);
	GetEntPropVector(0, Prop_Send, "m_WorldMins", fMin);
	
	fTop[2] = fMax[2]-6.0;
}

stock void TraceWater(float fPos[3], float fAng[3], float pos[3], float fNormal[3] = {0.0, 0.0, 0.0})
{
	TR_TraceRayFilter(fPos, fAng, MASK_WATER, RayType_Infinite, TraceEntityFilterPlayer);
	if(TR_DidHit(INVALID_HANDLE))
	{
		TR_GetEndPosition(pos, INVALID_HANDLE);
		TR_GetPlaneNormal(INVALID_HANDLE, fNormal);
		GetVectorAngles(fNormal, fNormal);
	}
}

public bool TraceEntityFilterPlayer(entity, contentsMask)
{
	return (entity > MaxClients || !entity);
}

void SpawnTesla(float pos[3])
{
	int entity = CreateEntityByName("point_tesla");
	DispatchKeyValue(entity, "m_flRadius", "70.0");
	DispatchKeyValue(entity, "m_SoundName", "DoSpark");
	DispatchKeyValue(entity, "beamcount_min", "42");
	DispatchKeyValue(entity, "beamcount_max", "62");
	DispatchKeyValue(entity, "texture", "sprites/physbeam.vmt");
	DispatchKeyValue(entity, "m_Color", "255 255 255");
	DispatchKeyValue(entity, "thick_min", "10.0");
	DispatchKeyValue(entity, "thick_max", "11.0");
	DispatchKeyValue(entity, "lifetime_min", "0.3");
	DispatchKeyValue(entity, "lifetime_max", "0.3");
	DispatchKeyValue(entity, "interval_min", "0.1");
	DispatchKeyValue(entity, "interval_max", "0.2");
	DispatchSpawn(entity);

	TeleportEntity(entity, pos, NULL_VECTOR, NULL_VECTOR);

	AcceptEntityInput(entity, "TurnOn");
	AcceptEntityInput(entity, "DoSpark");
	RemoveEntity(entity, 1.0);
}

void StopPlayer(int client)
{
	float fVel[3];
	TeleportEntity(client, NULL_VECTOR, NULL_VECTOR, fVel);
}

/* Available icons
	"icon_bulb"
	"icon_caution"
	"icon_alert"
	"icon_alert_red"
	"icon_tip"
	"icon_skull"
	"icon_no"
	"icon_run"
	"icon_interact"
	"icon_button"
	"icon_door"
	"icon_arrow_plain"
	"icon_arrow_plain_white_dn"
	"icon_arrow_plain_white_up"
	"icon_arrow_up"
	"icon_arrow_right"
	"icon_fire"
	"icon_present"
	"use_binding"
*/

stock void DisplayInstructorHint(int iTargetEntity, float fTime, float fHeight, float fRange, bool bFollow, bool bShowOffScreen, char[] sIconOnScreen, char[] sIconOffScreen, char[] sCmd, bool bShowTextAlways, int iColor[3], char sText[100])
{
	int iEntity = CreateEntityByName("env_instructor_hint");
	
	if(iEntity <= 0)
		return;
		
	char sBuffer[32];
	FormatEx(sBuffer, sizeof(sBuffer), "%d", iTargetEntity);
	
	// Target
	DispatchKeyValue(iTargetEntity, "targetname", sBuffer);
	DispatchKeyValue(iEntity, "hint_target", sBuffer);
	
	// Static
	FormatEx(sBuffer, sizeof(sBuffer), "%d", !bFollow);
	DispatchKeyValue(iEntity, "hint_static", sBuffer);
	
	// Timeout
	FormatEx(sBuffer, sizeof(sBuffer), "%d", RoundToFloor(fTime));
	DispatchKeyValue(iEntity, "hint_timeout", sBuffer);
	if(fTime > 0.0)
		RemoveEntity(iEntity, fTime);
	
	// Height
	FormatEx(sBuffer, sizeof(sBuffer), "%d", RoundToFloor(fHeight));
	DispatchKeyValue(iEntity, "hint_icon_offset", sBuffer);
	
	// Range
	FormatEx(sBuffer, sizeof(sBuffer), "%d", RoundToFloor(fRange));
	DispatchKeyValue(iEntity, "hint_range", sBuffer);
	
	// Show off screen
	FormatEx(sBuffer, sizeof(sBuffer), "%d", !bShowOffScreen);
	DispatchKeyValue(iEntity, "hint_nooffscreen", sBuffer);
	
	// Icons
	DispatchKeyValue(iEntity, "hint_icon_onscreen", sIconOnScreen);
	DispatchKeyValue(iEntity, "hint_icon_onscreen", sIconOffScreen);
	
	// Command binding
	DispatchKeyValue(iEntity, "hint_binding", sCmd);
	
	// Show text behind walls
	FormatEx(sBuffer, sizeof(sBuffer), "%d", bShowTextAlways);
	DispatchKeyValue(iEntity, "hint_forcecaption", sBuffer);
	
	// Text color
	FormatEx(sBuffer, sizeof(sBuffer), "%d %d %d", iColor[0], iColor[1], iColor[2]);
	DispatchKeyValue(iEntity, "hint_color", sBuffer);
	
	//Text
	ReplaceString(sText, sizeof(sText), "\n", " ");
	DispatchKeyValue(iEntity, "hint_caption", sText);
	
	DispatchSpawn(iEntity);
	AcceptEntityInput(iEntity, "ShowHint");
}

void SpawnFuncPrecipitation(int type = 0, float time = 10.0, int rgba[4] = {75, 255, 75, 200})
{
	float fMax[3];
	float fMin[3];
	float fTop[3];
	float center[3];
	
	GetEntPropVector(0, Prop_Send, "m_WorldMaxs", fMax);
	GetEntPropVector(0, Prop_Send, "m_WorldMins", fMin);
	
	fTop[2] = fMax[2]-6.0;
	
	for (int i = 0; i < 3; i++)
		center[i] = (fMin[i] + fMax[i]) / 2.0;
	
	int entity = CreateEntityByName("func_precipitation");

	char map[64];
	char formattedMap[PLATFORM_MAX_PATH];

	GetCurrentMap(map, sizeof(map));
	Format(formattedMap, sizeof(formattedMap), "maps/%s.bsp", map);

	SetEntPropVector(entity, Prop_Send, "m_vecMins", fMin);
	SetEntPropVector(entity, Prop_Send, "m_vecMaxs", fMax);

	DispatchKeyValue(entity, "model", formattedMap);
	
	char buffer[128];
	
	Format(buffer, sizeof(buffer), "%d", type);
	DispatchKeyValue(entity, "preciptype", buffer);
	
	Format(buffer, sizeof(buffer), "%d", rgba[3]);
	DispatchKeyValue(entity, "renderamt", buffer);
	
	Format(buffer, sizeof(buffer), "%d %d %d", rgba[0], rgba[1], rgba[2]);
	DispatchKeyValue(entity, "rendercolor", buffer);

	DispatchSpawn(entity);
	
	TeleportEntity(entity, center, NULL_VECTOR, NULL_VECTOR);
		
	RemoveEntity(entity, time);
}

/*
stock void PlaySoundWithSpeaker(int iClient, char[] soundPath, float fPos[3], int sndCh = SNDCHAN_AUTO, int sndLvl = SNDLEVEL_NORMAL, int sndFlags = SND_NOFLAGS, float sndVol = SNDVOL_NORMAL, int sndPitch = SNDPITCH_NORMAL)
{
	Handle hFile = OpenSoundFile(soundPath);
	
	if (hFile == null)
		return;
	
	float fLength = GetSoundLengthFloat(hFile)+1.0;
	
	delete hFile; 
	
	//int iEntity = SpawnSpeakerEntity(fPos, fLength);
	
	if(iClient != 0)
	{
		char sClient[16];
		Format(sClient, 16, "client%d", iClient);
		DispatchKeyValue(iClient, "targetname", sClient);
		
		SetVariantString(sClient);
		AcceptEntityInput(iEntity, "SetParent", iEntity, iEntity, 0);
	}

	EmitSoundToAllAny(soundPath, iClient, sndCh, sndLvl, sndFlags, sndVol, sndPitch, iClient, fPos, NULL_VECTOR, true);
}

stock int SpawnSpeakerEntity(float fPos[3], float ttl)
{
	int iEntity = CreateEntityByName("prop_physics_override");
	
	if(!IsModelPrecached("models/error.mdl"))
		PrecacheModel("models/error.mdl");
	SetEntityModel(iEntity, "models/error.mdl");
	SetEntityRenderMode(iEntity, RENDER_NONE);
	if(iEntity != -1)
	{
		TeleportEntity(iEntity, fPos, NULL_VECTOR, NULL_VECTOR);
		RemoveEntity(iEntity, ttl);
	}
	
	return iEntity;
}
*/

stock void RemoveEntity(iEntity, float time = 0.0)
{
	if (time == 0.0)
	{
		if (IsValidEntity(iEntity))
		{
			char edictname[32];
			GetEdictClassname(iEntity, edictname, 32);

			if (!StrEqual(edictname, "player"))
				AcceptEntityInput(iEntity, "kill");
		}
	}
	else if(time > 0.0)
		CreateTimer(time, RemoveEntityTimer, EntIndexToEntRef(iEntity), TIMER_FLAG_NO_MAPCHANGE);
}

public Action RemoveEntityTimer(Handle Timer, any entityRef)
{
	int entity = EntRefToEntIndex(entityRef);
	if (entity != INVALID_ENT_REFERENCE)
		RemoveEntity(entity); // RemoveEntity(...) is capable of handling references
	
	return (Plugin_Stop);
}

stock void GetSuffix(int number, char sSuffix[4], int client)
{
	int iLastDigit = number % 10;
	
	switch (iLastDigit)
	{
		case 1:
		{
			Format(sSuffix, sizeof(sSuffix), "%T", "Suffix_1", client);
		}
		case 2:
		{
			Format(sSuffix, sizeof(sSuffix), "%T", "Suffix_2", client);
		}
		case 3:
		{
			Format(sSuffix, sizeof(sSuffix), "%T", "Suffix_3", client);
		}
		default: 
		{  
			Format(sSuffix, sizeof(sSuffix), "%T", "Suffix_Default", client);
		}
	}
}

stock void PerformBlind(int client, int amount = 0)
{
	int targets[2];
	targets[0] = client;
	
	int duration = 1536;
	int holdtime = 1536;
	int flags;
	if (amount == 0)
		flags = (FFADE_IN | FFADE_PURGE);
	else flags = (FFADE_OUT | FFADE_STAYOUT);
	
	int color[4] = { 0, 0, 0, 0 };
	color[3] = amount;
	
	Handle message = StartMessageEx(g_FadeUserMsgId, targets, 1);
	if (GetUserMessageType() == UM_Protobuf)
	{
		PbSetInt(message, "duration", duration);
		PbSetInt(message, "hold_time", holdtime);
		PbSetInt(message, "flags", flags);
		PbSetColor(message, "clr", color);
	}
	else
	{
		BfWriteShort(message, duration);
		BfWriteShort(message, holdtime);
		BfWriteShort(message, flags);		
		BfWriteByte(message, color[0]);
		BfWriteByte(message, color[1]);
		BfWriteByte(message, color[2]);
		BfWriteByte(message, color[3]);
	}
	
	EndMessage();
}

stock void AddInFrontOf(float vecOrigin[3], float vecAngle[3], float units, float output[3])
{
	float vecAngVectors[3];
	vecAngVectors = vecAngle; //Don't change input
	GetAngleVectors(vecAngVectors, vecAngVectors, NULL_VECTOR, NULL_VECTOR);
	for (int i; i < 3; i++)
	output[i] = vecOrigin[i] + (vecAngVectors[i] * units);
}

public int MenuHandler_Empty(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_End)
		delete menu;
}

stock void Entity_SetNonMoveable(int entity)
{
	SetEntData(entity, g_iFreeze, FL_CLIENT|FL_ATCONTROLS, 4, true);
}

stock void Entity_SetMoveable(int entity)
{
	SetEntData(entity, g_iFreeze, FL_FAKECLIENT|FL_ONGROUND|FL_PARTIALGROUND, 4, true);
}

stock int Client_GetObservedPlayer(int client)
{
	int iObserverMode = GetEntProp(client, Prop_Send, "m_iObserverMode");
	if(iObserverMode == SPECMODE_FIRSTPERSON || iObserverMode == SPECMODE_3RDPERSON)
	{
		int iTarget = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
		
		if(iTarget > 0 || iTarget <= MaxClients)
			return iTarget;
	}
	return -1;
}

/**
 * Returns the next or previous client by id.
 *
 * @param client        Client from which the searching begins
 * @param fw            If false, returns a previous client instead of next.
 * @param team          Limit searching to a specific team, or -1 to search any.
 * @return The next or previous client.
 */
stock int GetNextClient(int client, bool fw=true, int team=-1)
{
	int d = (fw ? 1 : -1);
	int i = client + d;
	int begin = (fw ? 1 : MaxClients);
	int limit = (fw ? MaxClients + 1 : 0);
	
	int ttl = MaxClients;
	
	while (ttl > 0 && !IsClientInTeam(i, team))
	{
		ttl--;
		// move index; if index == limit, move it to the beginning
		i = (i + d == limit ? begin : i + d);
		
		// we made a full circle. no suitable client found
		if (i == client)
			return -1;
	}
	
	return i;
}

stock bool IsClientInTeam(int client, int team)
{
	return client > 0
			&& IsClientInGame(client)
			&& IsPlayerAlive(client)
			&& (GetClientTeam(client) == team || team == -1);
}

stock int GetRandomClient(int team, bool onlyAlive=false, bool onlyNonSwitching=false)
{
	int clientIndex;
	int clients[MAXPLAYERS+1];
	LoopIngameClients(i)
	{
		if (GetClientTeam(i) == team && (IsPlayerAlive(i) || !onlyAlive))
		{
			if (!onlyNonSwitching)
			{
				clients[clientIndex] = i;
				clientIndex++;
			}
		}
	}
	
	return clients[GetRandomInt(0, clientIndex-1)];
}

stock bool HG_IsClientAlive(int client)
{
	return g_bIsTribute[client] || g_bIsZombie[client];
}