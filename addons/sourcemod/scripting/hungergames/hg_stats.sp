#define CHARSET "utf8mb4"

/* Tables */

char sqlCreateTableRounds[] = "CREATE TABLE IF NOT EXISTS `hg_rounds` ( `id` int(11) NOT NULL PRIMARY KEY AUTO_INCREMENT, `map` varchar(64) NOT NULL, `winnerID` bigint NOT NULL, `winnerName` varchar(64) NOT NULL, `date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP) ENGINE=InnoDB DEFAULT CHARSET=" ... CHARSET ... ";";
char sqlCreateTablePlayers[] = "CREATE TABLE IF NOT EXISTS `hg_players` ( `playerID` bigint NOT NULL PRIMARY KEY, `wins` int(11) NOT NULL DEFAULT '0', `points` int(11) NOT NULL DEFAULT '0', `elo` int(11) NOT NULL DEFAULT '1000', `eloMin` int(11) NOT NULL DEFAULT '1000', `eloMax` int(11) NOT NULL DEFAULT '1000', `kills` int(11) NOT NULL DEFAULT '0', `knifekills` int(11) NOT NULL DEFAULT '0', `deaths` int(11) NOT NULL DEFAULT '0', `playtime` int(11) NOT NULL DEFAULT '0', `firstplay` int(11) NOT NULL DEFAULT '0', `lastplay` int(11) NOT NULL DEFAULT '0', `name` varchar(64) NOT NULL) ENGINE = InnoDB DEFAULT CHARSET = " ... CHARSET ... ";";
char sqlLoadLastRound[] = "SELECT MAX(`id`) FROM `hg_rounds`";
char sqlLoadPlayerCount[] = "SELECT COUNT(*) FROM `hg_players`";

/* Database */

bool g_bConnected;

/* Global Stats */

int g_iTotalPlayers;

/* Player */

char g_sAuth[MAXPLAYERS + 1][32];
bool g_bAuthed[MAXPLAYERS + 1];
bool g_bLoadedSQL[MAXPLAYERS + 1];

int g_iWinRank[MAXPLAYERS + 1];
int g_iEloRank[MAXPLAYERS + 1];
int g_iPointsRank[MAXPLAYERS + 1];

int g_iPoints[MAXPLAYERS + 1];
int g_iPointsTemp[MAXPLAYERS + 1];
int g_iWins[MAXPLAYERS + 1];
int g_iKills[MAXPLAYERS + 1];
int g_iKnifeKills[MAXPLAYERS + 1];
int g_iDeaths[MAXPLAYERS + 1];
int g_iElo[MAXPLAYERS + 1];
int g_iEloMin[MAXPLAYERS + 1];
int g_iEloMax[MAXPLAYERS + 1];
int g_iPlaytime[MAXPLAYERS + 1];
int g_iPlaytimeTemp[MAXPLAYERS + 1];
int g_iFirstplay[MAXPLAYERS + 1];
int g_iLastplay[MAXPLAYERS + 1];

public void CallBack_Empty(Handle owner, Handle hndl, const char[] error, any userid)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_Empty", error);
		return;
	}
}

#include "hungergames/stats/get.sp"
#include "hungergames/stats/update.sp"

#include "hungergames/stats/playtime.sp"
#include "hungergames/stats/elo.sp"

#include "hungergames/stats/statsme.sp"
#include "hungergames/stats/top.sp"

public void ZCore_Mysql_OnDatabaseError (int index, char[] config, char[] error )
{
	if(!StrEqual(config, "hg"))
		return;
	
	DatabaseError("ZCore_Mysql_OnDatabaseError", error);
}

void DatabaseError(const char[] sFunction, const char[] error)
{
	LogError("[hg.smx] SQL Error on %s: %s", sFunction, error);
	
	g_iSQL = -1;
	g_hSQL = null;
	g_bConnected = false;
}

public void ZCore_Mysql_OnDatabaseConnected(int index, char[] config, Database connection_handle)
{
	if(!StrEqual(config, "hg"))
		return;
	
	g_iSQL = index;
	g_hSQL = connection_handle;
	
	g_bConnected = true;
	CreateTables();
}

void ConnectDB()
{
	if(!g_pZcoreMysql)
		return;
		
	if(g_hSQL != null)
		return;
	
	ZCore_Mysql_Connect("hg");
	
	if(g_hSQL != null)
	{
		g_bConnected = true;
		CreateTables();
	}
}

void CreateTables()
{
	SQL_SetCharset(g_hSQL, CHARSET);
	SQL_TQuery(g_hSQL, CallBack_Empty, "SET NAMES '" ... CHARSET ... "';");
	
	SQL_TQuery(g_hSQL, CallBack_Empty, sqlCreateTableRounds);
	SQL_TQuery(g_hSQL, CallBack_Empty, sqlCreateTablePlayers);
	
	SQL_TQuery(g_hSQL, CallBack_LoadPlayerCount, sqlLoadPlayerCount);
	SQL_TQuery(g_hSQL, CallBack_LoadLastRound, sqlLoadLastRound);
}

public void CallBack_LoadPlayerCount(Handle owner, Handle hndl, const char[] error, any data)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadPlayerCount", error);
		return;
	}
	
	if(SQL_FetchRow(hndl))
		g_iTotalPlayers = SQL_FetchInt(hndl, 0);
	else g_iTotalPlayers = 0;
}

public void CallBack_LoadLastRound(Handle owner, Handle hndl, const char[] error, any data)
{
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadLastRound", error);
		return;
	}
	
	if(SQL_FetchRow(hndl))
		g_iAnnual = SQL_FetchInt(hndl, 0);
	else g_iAnnual = 0;
}

void ResetPlayer(int client)
{
	g_bAuthed[client] = false;
	g_bLoadedSQL[client] = false;
	
	g_iWinRank[client] = 0;
	g_iEloRank[client] = 0;
	g_iPointsRank[client] = 0;
		
	g_iPoints[client] = 0;
	g_iWins[client] = 0;
	g_iKills[client] = 0;
	g_iKnifeKills[client] = 0;
	g_iDeaths[client] = 0;
	g_iElo[client] = 1000;
	g_iEloMin[client] = 1000;
	g_iEloMax[client] = 1000;
	g_iPlaytime[client] = 0;
	g_iFirstplay[client] = 0;
	g_iLastplay[client] = 0;
}

int g_iLastConnect;

Handle g_hAuthTimer[MAXPLAYERS+1] = {null, ...};

void ResetAuthTimers()
{
	LoopClients(i)
		g_hAuthTimer[i] = null;
}

void CreateAuthTimer(int client)
{
	if(g_hAuthTimer[client] != null)
		CloseHandle(g_hAuthTimer[client]);
	
	DataPack pack;
	
	g_hAuthTimer[client] = CreateDataTimer(1.5, Timer_Auth, pack, TIMER_FLAG_NO_MAPCHANGE);
	
	pack.WriteCell(client);
	pack.WriteCell(GetClientUserId(client));
}

public Action Timer_Auth(Handle timer, Handle pack)
{
	ResetPack(pack);
	
	int client = ReadPackCell(pack);
	
	g_hAuthTimer[client] = null;
	
	if(IsClientInGame(client))
	{
		AuthPlayer(client);
		
		if(client != GetClientOfUserId(ReadPackCell(pack)))
			LogMessage("%N is not the same player anymore. UserID changed during the authentication.", client);
	}
	
	return Plugin_Handled;
}
	
void AuthPlayer(client)
{
	if(IsFakeClient(client))
	{
		ResetPlayer(client)
		return;
	}
	
	g_bAuthed[client] = false;
	g_bLoadedSQL[client] = false;
	
	if(!g_bConnected || !g_pZcoreMysql)
	{
		// Don'T spam this if manny players connect at the same time
		if(GetTime() - g_iLastConnect > 1.0)
		{
			g_iLastConnect = GetTime();
			ConnectDB();
		}
		
		CreateAuthTimer(client);
	}
	
	g_bAuthed[client] = GetClientAuthId(client, AuthId_SteamID64, g_sAuth[client], 32);
	
	if(g_bAuthed[client])
		LoadStats(client);
	else CreateAuthTimer(client);
}

void LoadStats(int client)
{
	if(!g_bConnected || !g_pZcoreMysql)
		return;
	
	g_bLoadedSQL[client] = false;
	
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "SELECT `points`,`wins`,`kills`,`knifekills`,`deaths`,`elo`,`eloMin`,`eloMax`,`playtime`,`firstplay`,`lastplay` FROM `hg_players` WHERE `playerID` = %s", g_sAuth[client]);
	SQL_TQuery(g_hSQL, CallBack_LoadStats, sQuery, GetClientUserId(client));
}

public void CallBack_LoadStats(Handle owner, Handle hndl, const char[] error, any userid)
{
	// Player disconnected
	int client = GetClientOfUserId(userid);
	if(!client || !IsClientInGame(client))
		return;
		
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadStats", error);
		return;
	}
	
	int iTime = GetTime();
	
	char sName[MAX_NAME_LENGTH];
	GetClientName(client, sName, sizeof(sName));
	
	char sSafeName[((MAX_NAME_LENGTH * 2) + 1)];
	SQL_EscapeString(g_hSQL, sName, sSafeName, sizeof(sSafeName));
	
	char sQuery[2048];
	
	// Create Client
	
	if(!SQL_GetRowCount(hndl))
	{
		// Count new player
		
		g_iTotalPlayers++;
		
		// Set default stats
	
		g_iWinRank[client] = 0;
		g_iEloRank[client] = 0;
		g_iPointsRank[client] = 0;
		
		g_iPoints[client] = 0;
		g_iWins[client] = 0;
		g_iKills[client] = 0;
		g_iKnifeKills[client] = 0;
		g_iDeaths[client] = 0;
		g_iElo[client] = 1000;
		g_iEloMin[client] = 1000;
		g_iEloMax[client] = 1000;
		g_iPlaytime[client] = 0;
		g_iFirstplay[client] = iTime;
		g_iLastplay[client] = iTime;
		
		// Add to database
		
		FormatEx(sQuery, sizeof(sQuery), "INSERT INTO `hg_players` (playerID, name, firstplay, lastplay) VALUES (%s, '%s', %d, %d)", g_sAuth[client], sSafeName, iTime, iTime);
		SQL_TQuery(g_hSQL, CallBack_CreateClient, sQuery, userid);
	}
	
	// Play exist already
	
	else if(SQL_FetchRow(hndl))
	{
		// Update player name
		
		FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET name = '%s', lastplay = %d WHERE playerID = %s", sSafeName, iTime, g_sAuth[client]);
		SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
	
		g_iWinRank[client] = 0;
		g_iEloRank[client] = 0;
		g_iPointsRank[client] = 0;
		
		g_iPoints[client] = SQL_FetchInt(hndl, 0);
		g_iWins[client] = SQL_FetchInt(hndl, 1);
		g_iKills[client] = SQL_FetchInt(hndl, 2);
		g_iKnifeKills[client] = SQL_FetchInt(hndl, 3);
		g_iDeaths[client] = SQL_FetchInt(hndl, 4);
		g_iElo[client] = SQL_FetchInt(hndl, 5);
		g_iEloMin[client] = SQL_FetchInt(hndl, 6);
		g_iEloMax[client] = SQL_FetchInt(hndl, 7);
		g_iPlaytime[client] = SQL_FetchInt(hndl, 8);
		g_iFirstplay[client] = SQL_FetchInt(hndl, 9);
		g_iLastplay[client] = SQL_FetchInt(hndl, 10);
		
		LoadRanks(client);
	}
}

public void CallBack_CreateClient(Handle owner, Handle hndl, const char[] error, any userid)
{
	// Player disconnected
	int client = GetClientOfUserId(userid);
	if(!client || !IsClientInGame(client))
		return;
		
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_CreateClient", error);
		return;
	}
	
	LoadRanks(client);
}

void LoadRanks(int client)
{
	if(!g_bConnected || !g_pZcoreMysql)
		return;
	
	char sQuery[2048];
	FormatEx(sQuery, sizeof(sQuery), "SELECT COUNT(*) FROM `hg_players` WHERE `points` >= %d ORDER BY `points` DESC", g_iPoints[client]);
	SQL_TQuery(g_hSQL, CallBack_LoadPointsRank, sQuery, GetClientUserId(client));
	
	FormatEx(sQuery, sizeof(sQuery), "SELECT COUNT(*) FROM `hg_players` WHERE `elo` >= %d ORDER BY `elo` DESC", g_iElo[client]);
	SQL_TQuery(g_hSQL, CallBack_LoadEloRank, sQuery, GetClientUserId(client));
	
	FormatEx(sQuery, sizeof(sQuery), "SELECT COUNT(*) FROM `hg_players` WHERE `wins` >= %d ORDER BY `wins` DESC", g_iWins[client]);
	SQL_TQuery(g_hSQL, CallBack_LoadWinRank, sQuery, GetClientUserId(client));
}

public void CallBack_LoadPointsRank(Handle owner, Handle hndl, const char[] error, any userid)
{
	int client = GetClientOfUserId(userid);
	if(!client || !IsClientInGame(client))
		return;
	
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadPointsRank", error);
		return;
	}
	
	if(SQL_FetchRow(hndl))
		g_iPointsRank[client] = SQL_FetchInt(hndl, 0);
	else g_iPointsRank[client] = 0;
}

public void CallBack_LoadEloRank(Handle owner, Handle hndl, const char[] error, any userid)
{
	int client = GetClientOfUserId(userid);
	if (!client || !IsClientInGame(client))
		return;
	
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadEloRank", error);
		return;
	}
	
	if(SQL_FetchRow(hndl))
		g_iEloRank[client] = SQL_FetchInt(hndl, 0);
	else g_iEloRank[client] = 0;
}

public void CallBack_LoadWinRank(Handle owner, Handle hndl, const char[] error, any userid)
{
	int client = GetClientOfUserId(userid);
	if(!client || !IsClientInGame(client))
		return;
	
	if (owner == null || hndl == null)
	{
		DatabaseError("CallBack_LoadWinRank", error);
		return;
	}
	
	if(SQL_FetchRow(hndl))
		g_iWinRank[client] = SQL_FetchInt(hndl, 0);
	else g_iWinRank[client] = 0;
	
	PlayerLoaded(client);
}

void PlayerLoaded(int client)
{
	g_bLoadedSQL[client] = true;
	
	char sAdr[32];		
	GetClientIP(client, sAdr, 32);
	
	char sCountry[32];
	Format(sCountry, 32, "Unknown");
	
	GeoipCountry(sAdr, sCountry, 32);     
	if(!strcmp(sCountry, NULL_STRING))
		Format(sCountry, 32, "The Moon", sCountry );
	else if(StrContains( sCountry, "United", false ) != -1 || 
			StrContains( sCountry, "Republic", false ) != -1 || 
			StrContains( sCountry, "Federation", false ) != -1 || 
			StrContains( sCountry, "Island", false ) != -1 || 
			StrContains( sCountry, "Netherlands", false ) != -1 || 
			StrContains( sCountry, "Isle", false ) != -1 || 
			StrContains( sCountry, "Bahamas", false ) != -1 || 
			StrContains( sCountry, "Maldives", false ) != -1 || 
			StrContains( sCountry, "Philippines", false ) != -1 || 
			StrContains( sCountry, "Vatican", false ) != -1 )
		Format( sCountry, 32, "The %s", sCountry );
	
	LoopIngameClients(i)
	{
		char sMsg[512];
		Format(sMsg, sizeof(sMsg), "%T", "ConnectMsg", i);
		
		ReplaceString(sMsg, sizeof(sMsg), "{PREFIX}", g_sPrefix);
		ReplaceString(sMsg, sizeof(sMsg), "{COUNTRY}", sCountry);
		
		char sBuffer2[32];
		
		IntToString(g_iWinRank[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{WINRANK}", sBuffer2);
		
		IntToString(g_iEloRank[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{ELORANK}", sBuffer2);
		
		IntToString(g_iPointsRank[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{POINTSRANK}", sBuffer2);
		
		IntToString(g_iTotalPlayers, sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{PLAYERS}", sBuffer2);
		
		IntToString(g_iWins[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{WINS}", sBuffer2);
		
		IntToString(g_iElo[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{ELO}", sBuffer2);
		
		IntToString(g_iEloMin[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{ELOMIN}", sBuffer2);
		
		IntToString(g_iEloMax[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{ELOMAX}", sBuffer2);
		
		IntToString(g_iPoints[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{POINTS}", sBuffer2);
		
		IntToString(g_iKills[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{KILLS}", sBuffer2);
		
		IntToString(g_iKnifeKills[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{KNIFEKILLS}", sBuffer2);
		
		IntToString(g_iDeaths[client], sBuffer2, sizeof(sBuffer2));
		ReplaceString(sMsg, sizeof(sMsg), "{DEATHS}", sBuffer2);
		
		char sName[MAX_NAME_LENGTH];
		GetClientName(client, sName, sizeof(sName));
		ReplaceString(sMsg, sizeof(sMsg), "{NAME}", sName);
		
		CPrintToChat(i, sMsg);
	}
	
	CreateTimer(0.0, Timer_SetClanTag, GetClientOfUserId(client), TIMER_FLAG_NO_MAPCHANGE);
}

void Stats_Roundend(int winner)
{
	if(!g_bConnected || !g_pZcoreMysql)
		return;
	
	bool bWinner = (winner && IsClientInGame(winner) && !IsFakeClient(winner));
	
	int iTime = GetTime();
	
	char sName[MAX_NAME_LENGTH];
	char sSafeName[((MAX_NAME_LENGTH * 2) + 1)];
	
	if(bWinner)
	{
		GetClientName(winner, sName, sizeof(sName));
		SQL_EscapeString(g_hSQL, sName, sSafeName, sizeof(sSafeName));
	}
	
	char sCurrentMap[64];
	GetCurrentMap(sCurrentMap, sizeof(sCurrentMap));
	
	/* Log round */
	char sQuery[512];
	FormatEx(sQuery, sizeof(sQuery), "INSERT INTO `hg_rounds` (map, winnerID, winnerName) VALUES ('%s', %s, '%s')", sCurrentMap, (bWinner && g_bAuthed[winner]) ? g_sAuth[winner] : "0", bWinner ? sSafeName : "Capitol");
	SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
	
	// Proceed only with a valid winner
	if(!bWinner || !g_bLoadedSQL[winner])
		return;
	
	/* Update winner win count */
	
	if(Client_HasAdminFlags(winner, g_iVIPflags))
		g_iPoints[winner] = RoundToFloor(float(g_iPoints[winner])*g_fPointsVIP);
	else g_iPoints[winner] += g_iPointsWin;
	
	FormatEx(sQuery, sizeof(sQuery), "UPDATE `hg_players` SET wins = wins+1, lastplay = %d, points = points+%d, playtime = playtime+%d WHERE playerID = %s", iTime, g_iPointsWin+g_iPointsTemp[winner], g_iPlaytimeTemp[winner], g_sAuth[winner]);
	SQL_TQuery(g_hSQL, CallBack_Empty, sQuery);
	
	g_iWins[winner]++;
	g_iPointsTemp[winner] = 0;
	g_iPlaytimeTemp[winner] = 0;
	
	CreateTimer(0.0, Timer_SetClanTag, GetClientOfUserId(winner), TIMER_FLAG_NO_MAPCHANGE);
}

public Action Cmd_GivePoints(int iClient, int iArgs)
{
	if (iArgs != 2)
	{
		ReplyToCommand(iClient, "Usage: hg_give_points <#userid|name> <points>");
		return Plugin_Handled;
	}
	
	char target_string[65];
	char amount_string[65];
	
	GetCmdArg(1, target_string, sizeof(target_string));
	GetCmdArg(2, amount_string, sizeof(amount_string));
	
	int iAmount = StringToInt(amount_string);
	
	char target_name[MAX_TARGET_LENGTH];
	int target_list[MAXPLAYERS];
	bool tn_is_ml;
	int target_count;
	
	if((target_count = ProcessTargetString(target_string, iClient, target_list, MAXPLAYERS, COMMAND_FILTER_CONNECTED, target_name, sizeof(target_name), tn_is_ml)) <= 0)
	{
		ReplyToTargetError(iClient, target_count);
		return Plugin_Handled;
	}
	
	ReplyToCommand(iClient, "Updating points for %i player%s.", target_count, target_count > 1 ? "s" : "");
	
	for(int i = 0; i < target_count; i++)
	{
		if(!Stats_AddPoints(target_list[i], iAmount, true))
			ReplyToCommand(iClient, "Could not update points for player %N", target_list[i]);
		else ReplyToCommand(iClient, "Updated points for player %N", target_list[i]);
	}
	
	return Plugin_Handled;
}