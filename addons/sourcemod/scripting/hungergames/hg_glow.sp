int g_iColorTribute[3] =  {20, 20, 255};
int g_iColorParty[3] =  {20, 255, 20};
int g_iColorZombie[3] =  {255, 20, 20};

void StartGlowTimer()
{
	if(g_pCustomPlayerSkins)
		CreateTimer(0.5, Timer_SetupGlow, _, TIMER_REPEAT);
}

public Action Timer_SetupGlow(Handle timer, any data)
{
	if(RoundStatus < RS_InProgress || !g_pCustomPlayerSkins)
		return Plugin_Continue;
	
	LoopAlivePlayers(iClient)
		SetupGlowSkin(iClient)
	
	return Plugin_Continue;
}

void SetupGlowSkin(int iClient)
{
	UnHookSkin(iClient);
	CPS_RemoveSkin(iClient);
	
	if(IsKnockedOut(iClient))
	{
		SetEntityRenderMode(iClient, RENDER_NONE);
		return;
	}
	
	if(RoundStatus < RS_InProgress || IsKnockedOut(iClient))
		return;
	
	char model[PLATFORM_MAX_PATH];
	GetClientModel(iClient, model, sizeof(model));
	int skin = CPS_SetSkin(iClient, model, CPS_RENDER);
	
	if(skin == -1)
		return;
	if (SDKHookEx(skin, SDKHook_SetTransmit, OnSetTransmit_GlowSkin))
		SetupGlow(iClient, skin);
}

void UnHookSkin(int iClient)
{
	if(CPS_HasSkin(iClient))
	{
		int skin = EntRefToEntIndex(CPS_GetSkin(iClient));
		
		if(IsValidEntity(skin))
			SDKUnhook(skin, SDKHook_SetTransmit, OnSetTransmit_GlowSkin)
	}
}

void SetupGlow(int iClient, int skin)
{
	static int offset;
	
	if (!offset && (offset = GetEntSendPropOffs(skin, "m_clrGlow")) == -1)
		return;
	
	SetEntProp(skin, Prop_Send, "m_bShouldGlow", true, true);
	SetEntProp(skin, Prop_Send, "m_nGlowStyle", 0);
	SetEntPropFloat(skin, Prop_Send, "m_flGlowMaxDist", 10000000.0);
	
	int red = 255;
	int green = 255;
	int blue = 255;
	
	if(g_bIsTribute[iClient])
	{
		if(GetParty(iClient) != -1)
		{
			red = g_iColorParty[0];
			green = g_iColorParty[1];
			blue = g_iColorParty[2];
		}
		else 
		{
			red = g_iColorTribute[0];
			green = g_iColorTribute[1];
			blue = g_iColorTribute[2];
		}
	}
	else if(IsZombie(iClient))
	{
		red = g_iColorZombie[0];
		green = g_iColorZombie[1];
		blue = g_iColorZombie[2];
	}
	else
	{
		red = 0;
		green = 0;
		blue = 0;
	}
	
	SetEntData(skin, offset, red, _, true);
	SetEntData(skin, offset + 1, green, _, true);
	SetEntData(skin, offset + 2, blue, _, true);
	SetEntData(skin, offset + 3, 255, _, true);
}

public Action OnSetTransmit_GlowSkin(int skin, int iClient)
{
	// Bots and dead players can't see glow
	if (IsFakeClient(iClient) || !IsPlayerAlive(iClient) || !g_pCustomPlayerSkins)
		return Plugin_Handled;
	
	// Find owner of the glow model
	int iTarget = -1;
	int district = -2;
	bool allSameDistrict = true;
	LoopAlivePlayers(i)
	{
		if(district == -2)
			district = GetParty(i)
		else if((g_bIsTribute[i] || IsDelayedDeath(i)) && district != GetParty(i))
			allSameDistrict = false;
		
		if(!CPS_HasSkin(i))
			continue;
			
		if(EntRefToEntIndex(CPS_GetSkin(i)) != skin)
			continue;
		
		iTarget = i;
	}
	
	// Target not found?!?
	if(iTarget == -1)
		return Plugin_Handled;
	
	// Target is knocked out
	if(IsKnockedOut(iTarget))
		return Plugin_Handled;
	
	// zombies see all players
	if(IsZombie(iClient))
		return Plugin_Continue;
	
	// Target is tagged
	if(GetGameTime() < GetPlayerTagEndTime(iTarget))
		return Plugin_Continue;
	
	// Party members see mates
	if(!allSameDistrict && RoundStatus == RS_InProgress && GetParty(iClient) != -1 && GetParty(iClient) == GetParty(iTarget))
		return Plugin_Continue;
	
	return Plugin_Handled;
}