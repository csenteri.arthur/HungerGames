float g_fBlockPrimaryTools[MAXPLAYERS + 1];

void ResetBlockPrimaryTools(int client)
{
	g_fBlockPrimaryTools[client] = 0.0;
}

void BlockPrimaryTools(int client, float timetoblock)
{
	g_fBlockPrimaryTools[client] = GetGameTime() + timetoblock;
}

#include "hungergames/tools/binoculars.sp"
#include "hungergames/tools/compass.sp"
#include "hungergames/tools/flashlight.sp"
#include "hungergames/tools/parachute.sp"
#include "hungergames/tools/throwingknives.sp"

void ResetTools(int client)
{
	ResetBinoculars(client);
	ResetCompass(client);
	ResetJammer(client);
	ResetTracer(client);
	ResetGPS(client);
	ResetParachute(client);
	
	ResetGiveBinocularsTimer(client);
	ResetGiveCompassTimer(client);
	ResetGiveParachuteTimer(client);
}

bool HasEquipment(int client)
{
	if(GetThrowingKnifes(client) > 0)
		return true;
		
	if(HasBinoculars(client))
		return true;
		
	if(HasCompass(client))
		return true;
		
	if(HasTracer(client))
		return true;
		
	if(HasJammer(client))
		return true;
		
	if(HasParachute(client))
		return true;
	
	return false;
}