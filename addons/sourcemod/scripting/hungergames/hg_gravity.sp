// Credits: CoMaNdO
// https://forums.alliedmods.net/showthread.php?p=2135406

float g_fGravity[MAXPLAYERS + 1];
MoveType gMT_MoveType[MAXPLAYERS + 1];

stock void SetClientGravity(int client, float fGravity)
{
	g_fGravity[client] = fGravity;
	
	if(gMT_MoveType[client] != MOVETYPE_LADDER)
		SetEntityGravity(client, g_fGravity[client]);
}

public void OnGroundEntChanged(int client)
{
	if(IsClientInGame(client) && IsPlayerAlive(client))
	{
		MoveType MT_MoveType = GetEntityMoveType(client)
		float fGravity = GetEntityGravity(client);
		
		if(MT_MoveType == MOVETYPE_LADDER)
		{
			if(fGravity != 0.0)
				g_fGravity[client] = fGravity;
		}
		else
		{
			if(gMT_MoveType[client] == MOVETYPE_LADDER)
				SetEntityGravity(client, g_fGravity[client]);
			
			g_fGravity[client] = fGravity;
		}
		
		gMT_MoveType[client] = MT_MoveType;
	}
	else
	{
		g_fGravity[client] = 1.0;
		gMT_MoveType[client] = MOVETYPE_WALK;
	}
}