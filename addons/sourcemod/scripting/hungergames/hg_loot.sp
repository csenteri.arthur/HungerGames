enum HG_Loot
{
	Loot_Type, // The typeID we get from ZCore-LootSpawner
	
	// Interaction
	
	bool:Loot_Use,
	bool:Loot_Touch,
	Float:Loot_Defuse,
	Float:Loot_Lockpicking,
	
	Loot_TTL,
	Loot_Health,
	
	// General
	
	String:Loot_Name[32],
	String:Loot_Model[256],
	String:Loot_Sound[256],
	
	String:Loot_Msg[512],
	
	// Loot Spawning
	
	Loot_MaxSpawned,
	Loot_Weight,
	Loot_RoundStartWeight,
	Loot_DistanceMin,
	Loot_DistanceMax,
	
	// Weapons etc.
	
	String:Loot_LootTable[32],
	
	// Survival
	
	Float:Loot_Hunger,
	Float:Loot_Thirst,
	Float:Loot_Stamina,
	
	// Health/Armor
	
	Loot_GiveArmor,
	Loot_GiveHealth,
	
	// Tools
	
	bool:Loot_Binoculars,
	bool:Loot_Compass,
	bool:Loot_Tracer,
	bool:Loot_Jammer,
	bool:Loot_GPS,
	bool:Loot_Parachute,
	
	// Traps
	
	bool:Loot_ProxyMine,
	
	// Points
	
	Loot_PointsMin,
	Loot_PointsMax,
	
	// Shop
	Loot_ShopPrice,
	Loot_ShopPriceDead,
};

int g_eLoot[128][HG_Loot];
int g_iLootGroupCount;

void EmptyLoot()
{
	if(!g_pZcoreLootSpawner)
		return;
	
	for(int i = 0; i < g_iLootGroupCount; i++)
	{
		char sName[32]; 
		strcopy(sName, 32, g_eLoot[g_iLootGroupCount][Loot_Name]);
		
		char sModel[256];
		strcopy(sModel, 256, g_eLoot[g_iLootGroupCount][Loot_Model]);
		
		ZCore_LootSpawner_RegisterLootType(sName, sModel, 0, 0, 0, 0, 0, 0, 0, 0);
	}
}

void RegisterLoot()
{
	if(!g_pZcoreLootSpawner)
		return;
	
	Handle kvConfig = CreateKeyValues("Loot");
	
	char sPath[PLATFORM_MAX_PATH];
	BuildPath(Path_SM, sPath, sizeof(sPath), "configs/hg_loot.txt");
	FileToKeyValues(kvConfig, sPath);
	
	if(!KvGotoFirstSubKey(kvConfig))
		return;
	
	g_iLootGroupCount = 0;
	do
	{
		KvGetSectionName(kvConfig, g_eLoot[g_iLootGroupCount][Loot_Name], 32);
		
		KvGetString(kvConfig, "model", g_eLoot[g_iLootGroupCount][Loot_Model], 256, "");
		KvGetString(kvConfig, "sound", g_eLoot[g_iLootGroupCount][Loot_Sound], 256, "");
		
		KvGetString(kvConfig, "msg", g_eLoot[g_iLootGroupCount][Loot_Msg], 512, "");
		
		g_eLoot[g_iLootGroupCount][Loot_Use] = view_as<bool>(KvGetNum(kvConfig, "use", 0));
		g_eLoot[g_iLootGroupCount][Loot_Touch] = view_as<bool>(KvGetNum(kvConfig, "touch", 0));
		g_eLoot[g_iLootGroupCount][Loot_Defuse] = KvGetFloat(kvConfig, "defuse", 0.0);
		g_eLoot[g_iLootGroupCount][Loot_Lockpicking] = KvGetFloat(kvConfig, "lockpicking", 0.0);
		
		g_eLoot[g_iLootGroupCount][Loot_Weight] = KvGetNum(kvConfig, "weight", 1);
		g_eLoot[g_iLootGroupCount][Loot_RoundStartWeight] = KvGetNum(kvConfig, "round_start_weight", 1);
		g_eLoot[g_iLootGroupCount][Loot_MaxSpawned] = KvGetNum(kvConfig, "max_spawned", 100);
		
		g_eLoot[g_iLootGroupCount][Loot_DistanceMin] = KvGetNum(kvConfig, "distance_min", 0);
		g_eLoot[g_iLootGroupCount][Loot_DistanceMax] = KvGetNum(kvConfig, "distance_max", 0);
		
		KvGetString(kvConfig, "loottable", g_eLoot[g_iLootGroupCount][Loot_LootTable], 32, "");
		LoadLootTable(g_eLoot[g_iLootGroupCount][Loot_LootTable]);
		
		g_eLoot[g_iLootGroupCount][Loot_Hunger] = KvGetFloat(kvConfig, "hunger", 0.0);
		g_eLoot[g_iLootGroupCount][Loot_Thirst] = KvGetFloat(kvConfig, "thirst", 0.0);
		g_eLoot[g_iLootGroupCount][Loot_Stamina] = KvGetFloat(kvConfig, "stamina", 0.0);
		
		g_eLoot[g_iLootGroupCount][Loot_GiveArmor] = KvGetNum(kvConfig, "armor", 0);
		g_eLoot[g_iLootGroupCount][Loot_GiveHealth] = KvGetNum(kvConfig, "health", 0);
		
		g_eLoot[g_iLootGroupCount][Loot_Binoculars] = view_as<bool>(KvGetNum(kvConfig, "binoculars", 0));
		g_eLoot[g_iLootGroupCount][Loot_Compass] = view_as<bool>(KvGetNum(kvConfig, "compass", 0));
		g_eLoot[g_iLootGroupCount][Loot_Tracer] = view_as<bool>(KvGetNum(kvConfig, "tracer", 0));
		g_eLoot[g_iLootGroupCount][Loot_Jammer] = view_as<bool>(KvGetNum(kvConfig, "jammer", 0));
		g_eLoot[g_iLootGroupCount][Loot_GPS] = view_as<bool>(KvGetNum(kvConfig, "gps", 0));
		g_eLoot[g_iLootGroupCount][Loot_Parachute] = view_as<bool>(KvGetNum(kvConfig, "parachute", 0));
		
		g_eLoot[g_iLootGroupCount][Loot_ProxyMine] = view_as<bool>(KvGetNum(kvConfig, "proxymine", 0));
		
		g_eLoot[g_iLootGroupCount][Loot_PointsMin] = KvGetNum(kvConfig, "points_min", 0);
		g_eLoot[g_iLootGroupCount][Loot_PointsMax] = KvGetNum(kvConfig, "points_max", 0);
		
		g_eLoot[g_iLootGroupCount][Loot_ShopPrice] = KvGetNum(kvConfig, "shop_price", 0);
		g_eLoot[g_iLootGroupCount][Loot_ShopPriceDead] = KvGetNum(kvConfig, "shop_price_dead", 0);
		
		// Precache sound
		
		PrecacheSoundAny(g_eLoot[g_iLootGroupCount][Loot_Sound]);
		
		char sBuffer[256];
		Format(sBuffer, sizeof(sBuffer), "sound/%s", g_eLoot[g_iLootGroupCount][Loot_Sound]);
		AddFileToDownloadsTable(sBuffer);
		
		// Register loot
		
		char sName[32];
		strcopy(sName, 32, g_eLoot[g_iLootGroupCount][Loot_Name]);
		
		int iFlags = LS_FLAG_USESPAWNCENTER;
		
		if(g_eLoot[g_iLootGroupCount][Loot_Use])
			iFlags |= LS_FLAG_USE;
			
		if(g_eLoot[g_iLootGroupCount][Loot_Touch])
			iFlags |= LS_FLAG_TOUCH;
		
		char sModel[256];
		strcopy(sModel, 256, g_eLoot[g_iLootGroupCount][Loot_Model]);
		g_eLoot[g_iLootGroupCount][Loot_Type] = ZCore_LootSpawner_RegisterLootType(sName, sModel, 0, 0, g_eLoot[g_iLootGroupCount][Loot_Weight], g_eLoot[g_iLootGroupCount][Loot_RoundStartWeight], g_eLoot[g_iLootGroupCount][Loot_MaxSpawned], iFlags, g_eLoot[g_iLootGroupCount][Loot_DistanceMin], g_eLoot[g_iLootGroupCount][Loot_DistanceMax]);
		
		g_iLootGroupCount++;
		
	} while (KvGotoNextKey(kvConfig));
	
	CloseHandle(kvConfig);
}

int GetLootIndex(int type)
{
	for (int i = 0; i < g_iLootGroupCount; i++)
	{
		if(g_eLoot[i][Loot_Type] != type)
			continue;
			
		return i;
	}
	
	return -1;
}

void AddArmor(int client, int value)
{
	GiveHelmet(client);
	
	int armor = GetEntProp(client, Prop_Send, "m_ArmorValue");
	
	armor += value;
	
	if(armor > 100)
		armor = 100;
	
	SetEntProp(client, Prop_Send, "m_ArmorValue", armor, 1);
}

void MineExplode(int entity)
{
	float pos[3];
	Entity_GetAbsOrigin(entity, pos);
	pos[2] += 32.0;
	
	Env_Explosion(0, -1, pos, "weapon_proxymine", g_iMineMagnitude, 0, 0.0, 0);
}

void GiveHelmet(int client)
{
	SetEntProp(client, Prop_Send, "m_bHasHelmet", 1);
}

void ResetArmor(int client)
{
	SetEntProp(client, Prop_Send, "m_ArmorValue", 0, 1);
	SetEntProp(client, Prop_Send, "m_bHasHelmet", 0);
}

void DeleteParachuteLoot()
{
	if(!g_pZcoreLootSpawner)
		return;
	
	for (int i = 0; i < g_iLootGroupCount; i++)
	{
		if(!g_eLoot[i][Loot_Parachute])
			continue;
		
		ZCore_LootSpawner_WipeEntitysByType(g_eLoot[i][Loot_Type]);
	}
}

void DeleteBinocularLoot()
{
	if(!g_pZcoreLootSpawner)
		return;
	
	for (int i = 0; i < g_iLootGroupCount; i++)
	{
		if(!g_eLoot[i][Loot_Binoculars])
			continue;
		
		ZCore_LootSpawner_WipeEntitysByType(g_eLoot[i][Loot_Type]);
	}
}

void DeleteCompassLoot()
{
	for (int i = 0; i < g_iLootGroupCount; i++)
	{
		if(!g_eLoot[i][Loot_Compass])
			continue;
		
		ZCore_LootSpawner_WipeEntitysByType(g_eLoot[i][Loot_Type]);
	}
}

public Action ZCore_LootSpawner_OnLootSpawned(int entity, int type)
{
	return Plugin_Continue;
}

public Action ZCore_LootSpawner_OnLootAction(int client, int entity, int type, int action)
{
	int index = GetLootIndex(type);
	
	// Is this a lootgroup we have registered?
	if(index == -1)
		return Plugin_Continue;
	
	if(action == LS_ACTION_ENDTOUCH)
	{
		if(g_eLoot[index][Loot_ProxyMine])
		{
			OpenLoot(client, index, entity);
			ZCore_LootSpawner_RemoveLoot(entity, client, LS_ACTION_REMOVE);
		}
	}
	
	if(IsZombie(client))
		return Plugin_Continue;
	
	if(action == LS_ACTION_STARTTOUCH)
	{
		if(!g_eLoot[index][Loot_ProxyMine])
		{
			OpenLoot(client, index, entity);
			ZCore_LootSpawner_RemoveLoot(entity, client, LS_ACTION_REMOVE);
		}
	}
	
	return Plugin_Continue; //Remove the entity
}

void OpenLoot(int client, int index, int entity)
{	
	if(!g_pZcoreLootSpawner)
		return;
	
	// Format msg
	
	char msg[512];
	Format(msg, 512, "%T", g_eLoot[index][Loot_Msg], client);
	
	ReplaceString(msg, 512, "{NAME}", g_eLoot[index][Loot_Name]);
	
	// Thirst
	
	if(g_eLoot[index][Loot_Thirst] != 0)
	{
		HG_AddThirst(client, g_eLoot[index][Loot_Thirst]);
		
		char sBuffer[32];
		IntToString(RoundToFloor(g_eLoot[index][Loot_Thirst]), sBuffer, 32);
		ReplaceString(msg, 512, "{THIRST}", sBuffer);
	}
	
	// Hunger
	
	if(g_eLoot[index][Loot_Hunger] != 0)
	{
		HG_AddHunger(client, g_eLoot[index][Loot_Hunger]);
		
		char sBuffer[32];
		IntToString(RoundToFloor(g_eLoot[index][Loot_Hunger]), sBuffer, 32);
		ReplaceString(msg, 512, "{HUNGER}", sBuffer);
	}
	
	// Stamina
		
	if(g_eLoot[index][Loot_Stamina] != 0)
	{
		HG_AddStamina(client, g_eLoot[index][Loot_Stamina]);
		
		char sBuffer[32];
		IntToString(RoundToFloor(g_eLoot[index][Loot_Stamina]), sBuffer, 32);
		ReplaceString(msg, 512, "{STAMINA}", sBuffer);
	}
	
	// Give Armor
	
	if(g_eLoot[index][Loot_GiveArmor] != 0)
	{
		AddArmor(client, g_eLoot[index][Loot_GiveArmor]);
		
		char sBuffer[32];
		IntToString(g_eLoot[index][Loot_GiveArmor], sBuffer, 32);
		ReplaceString(msg, 512, "{ARMOR}", sBuffer);
	}
		
	// Give Health
	
	if(g_eLoot[index][Loot_GiveHealth] != 0)
	{
		int health = GetClientHealth(client) + g_eLoot[index][Loot_GiveHealth];
		SetEntityHealth(client, health > 100 ? 100 : health);
		
		char sBuffer[32];
		IntToString(g_eLoot[index][Loot_GiveHealth], sBuffer, 32);
		ReplaceString(msg, 512, "{HEALTH}", sBuffer);
	}
	
	// Give weapon
	
	char sWeapon[32];
	GiveRandomWeapon(client, g_eLoot[index][Loot_LootTable], sWeapon);
	ReplaceString(msg, 512, "{WEAPON}", sWeapon);
	
	// Give tools
	
	if(g_eLoot[index][Loot_Binoculars])
		GiveBinoculars(client);
	
	if(g_eLoot[index][Loot_Compass])
		GiveCompass(client);
	
	if(g_eLoot[index][Loot_Tracer])
		GiveTracer(client);
	
	if(g_eLoot[index][Loot_Jammer])
		GiveJammer(client);
	
	if(g_eLoot[index][Loot_GPS])
		GiveGPS(client);
	
	if(g_eLoot[index][Loot_Parachute])
		GiveParachute(client);
	
	if(g_eLoot[index][Loot_ProxyMine])
		MineExplode(entity);
	
	// Send msg
	
	CPrintToChat(client, "%s %s", g_sPrefix, msg);
	
	// Play Sound
	
	if(!StrEqual(g_eLoot[index][Loot_Sound], ""))
	{
		float fPos[3];
		GetClientAbsOrigin(client, fPos);
		EmitAmbientSoundAny(g_eLoot[index][Loot_Sound], fPos);
	}
	
	// Points
	
	if(g_eLoot[index][Loot_PointsMin] > 0 && g_eLoot[index][Loot_PointsMax] >= g_eLoot[index][Loot_PointsMin])
	{
		int iPoints = GetRandomInt(g_eLoot[index][Loot_PointsMin], g_eLoot[index][Loot_PointsMax]);
		
		if(Client_HasAdminFlags(client, g_iVIPflags))
			iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
		
		Stats_AddPoints(client, iPoints, false);
	}
	
	BlockPrimaryTools(client, 1.5);
}

/* Defuse Loot */

int g_iDefusing[MAXPLAYERS + 1];
int g_iDefusingEntityRef[MAXPLAYERS + 1] = {INVALID_ENT_REFERENCE, ...};
float g_fDefusingStartTime[MAXPLAYERS + 1];

void CheckLoot(int client, int buttons, int weapon)
{
	if(!g_pZcoreLootSpawner)
		return;
	
	int iEntity = EntRefToEntIndex(g_iDefusingEntityRef[client]);
	
	if(!g_bIsTribute[client] || !IsPlayerAlive(client))
	{
		if(g_iDefusing[client] > 0)
		{
			if(g_iDefusing[client] == 1)
				CPrintToChat(client, "%T", "AbortLockpicking", client, g_sPrefix);
			else 
			{
				CPrintToChat(client, "%T", "AbortDefusing", client, g_sPrefix);
				
				if(iEntity != INVALID_ENT_REFERENCE)
					ZCore_LootSpawner_RemoveLoot(iEntity, client, LS_ACTION_REMOVE);
			}
				
			g_iDefusing[client] = 0;
			
			if(IsPlayerAlive(client))
			{
				BlockWeapon(client, weapon, 0.5);
				StopPlayer(client);
				SetEntityMoveType(client, MOVETYPE_WALK);
			}
		}
		return;
	}
	
	// Validate target
	if(iEntity == INVALID_ENT_REFERENCE)
	{
		// Abort defusing (no valid target)
		if(g_iDefusing[client] > 0)
		{
			if(g_iDefusing[client] == 1)
				CPrintToChat(client, "%T", "AbortLockpicking", client, g_sPrefix);
			else CPrintToChat(client, "%T", "AbortDefusing", client, g_sPrefix);
				
			g_iDefusing[client] = 0;
			BlockWeapon(client, weapon, 0.5);
			StopPlayer(client);
			SetEntityMoveType(client, MOVETYPE_WALK);
			return;
		}
		
		if(!(buttons & IN_USE))
			return;
		
		iEntity = GetClientAimTarget(client, false);
		
		if(iEntity < MaxClients)
			return;
		
		int type = ZCore_LootSpawner_GetType(iEntity);
		
		if(type == TYPE_NONE)
			return;
		
		int index = GetLootIndex(type);
		
		if(index == -1)
			return;
		
		if(g_eLoot[index][Loot_Defuse] <= 0.0 && g_eLoot[index][Loot_Lockpicking] <= 0.0)
			return;
		
		if(!Entity_InRange(client, iEntity, 100.0))
			return;
			
		// Start Defusing/Lockpicking
		
		SetEntityMoveType(iEntity, MOVETYPE_NONE);
		
		if(g_eLoot[index][Loot_Lockpicking] > 0.0)
		{
			g_iDefusing[client] = 1;
			CPrintToChat(client, "%T", "StartLockpicking", client, g_sPrefix);
		}
		else 
		{
			g_iDefusing[client] = 2;
			CPrintToChat(client, "%T", "StartDefusing", client, g_sPrefix);
		}
		
		BlockWeapon(client, weapon, 1.5);
		StopPlayer(client);
		SetEntityMoveType(client, MOVETYPE_NONE);
			
		g_fDefusingStartTime[client] = GetGameTime();
		g_iDefusingEntityRef[client] = EntIndexToEntRef(iEntity);
		
		return;
	}
	
	// Player is not defusing loot
	if(g_iDefusing[client] == 0)
		return;
	
	int type = ZCore_LootSpawner_GetType(iEntity);
	int index = GetLootIndex(type);
	
	// Could not find loot group
	if(index == -1)
		return;
	
	// Player abort defusing
	if(!(buttons & IN_USE) || !Entity_InRange(client, iEntity, 100.0))
	{
		if(g_iDefusing[client] == 1)
			CPrintToChat(client, "%T", "AbortLockpicking", client, g_sPrefix);
		else 
		{
			CPrintToChat(client, "%T", "AbortDefusing", client, g_sPrefix);
			ZCore_LootSpawner_RemoveLoot(iEntity, client, LS_ACTION_REMOVE);
		}
		
		BlockWeapon(client, weapon, 0.5);
		StopPlayer(client);
		SetEntityMoveType(client, MOVETYPE_WALK);
		
		if(g_bMineAbortExplode && g_iDefusing[client] == 2)
			OpenLoot(client, index, iEntity);
		else SetEntityMoveType(iEntity, MOVETYPE_WALK);
		
		g_iDefusing[client] = 0;
		g_iDefusingEntityRef[client] = INVALID_ENT_REFERENCE;
		
		return;
	}
	
	// Continue
	float fTime = GetGameTime();
	float fTimeDefusing = fTime - g_fDefusingStartTime[client];
	float fDefuseTime;
	
	if(g_iDefusing[client] == 1)
		fDefuseTime = g_eLoot[index][Loot_Lockpicking];
	else fDefuseTime = g_eLoot[index][Loot_Defuse];
	
	// Finished
	if(fTimeDefusing >= fDefuseTime)
	{
		if(g_iDefusing[client] == 1)
		{
			CPrintToChat(client, "%T", "Unlocked", client, g_sPrefix);
			OpenLoot(client, index, iEntity);
			ZCore_LootSpawner_RemoveLoot(iEntity, client, LS_ACTION_PICKUP);
		}
		else 
		{
			int iPoints = g_iPointsDefuseMine;
			
			if(Client_HasAdminFlags(client, g_iVIPflags))
				iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
			
			Stats_AddPoints(client, iPoints, true);
			CPrintToChat(client, "%T", "Defused", client, g_sPrefix);
			ZCore_LootSpawner_RemoveLoot(iEntity, client, LS_ACTION_REMOVE);
		}
		
		BlockWeapon(client, weapon, 0.5);
		StopPlayer(client);
		SetEntityMoveType(client, MOVETYPE_WALK);
		
		g_iDefusing[client] = 0;
	}
	else
	{
		/* Block other tools */
		BlockPrimaryTools(client, 1.0);
		
		/* Block weapons */
		BlockWeapon(client, weapon, 1.5);
		
		/* Restrict client view */
		float fPos[3];
		Entity_GetAbsOrigin(iEntity, fPos);
		LookAtPoint(client, fPos);
		
		char centerText[512];
		
		if(g_iDefusing[client] == 1)
			Format(centerText, sizeof(centerText), "<font size='24' color='#FF2222'><bold> %T", "Lockpicking", client);
		else Format(centerText, sizeof(centerText), "<font size='24' color='#FF2222'><bold> %T", "Defusing", client);
		
		PrintHintText(client, centerText);
	}
}

Handle g_hTimerWipeLoot = null;

void StartLootWipetimer()
{
	if(g_hTimerWipeLoot != null)
	{
		delete g_hTimerWipeLoot;
		g_hTimerWipeLoot = null;
	}
	
	if(g_fLoopWipeInterval > 0.0)
		g_hTimerWipeLoot = CreateTimer(g_fLoopWipeInterval, Timer_WipeLoot, g_iAnnual, TIMER_REPEAT);
}

public Action Timer_WipeLoot(Handle timer, any data)
{
	if(data != g_iAnnual || (RoundStatus != RS_InProgress && RoundStatus != RS_Endgame))
		return Plugin_Continue;
	
	ServerCommand("zcore_loot_reload");
	
	return Plugin_Continue;
}