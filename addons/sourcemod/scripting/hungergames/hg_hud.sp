int g_iFreezeTime;

Handle g_hHUDtimer = null;

void StartHUDTimer()
{
	if(g_hHUDtimer != null)
		CloseHandle(g_hHUDtimer);
	
	g_hHUDtimer = CreateTimer(g_fHudTickInterval, Timer_HUD, _, TIMER_REPEAT);
}

public Action Timer_HUD(Handle timer, any data)
{
	g_iFreezeTime = GetConVarInt(FindConVar("mp_freezetime"));
	
	LoopIngamePlayers(client)
	{
		int iClientToShow = client;
		
		// Check if players is spectating another player
		
		if(!IsPlayerAlive(client) || IsClientObserver(client))
		{
			int iObserverMode = GetEntProp(client, Prop_Send, "m_iObserverMode");
			if(iObserverMode == SPECMODE_FIRSTPERSON || iObserverMode == SPECMODE_3RDPERSON)
			{
				iClientToShow = GetEntPropEnt(client, Prop_Send, "m_hObserverTarget");
				
				if(iClientToShow <= 0 || iClientToShow > MaxClients)
					continue;
			}
			else continue;
		}
		
		SelectHUD(client, iClientToShow);
	}
	
	return Plugin_Continue;
}

void SelectHUD(int client, int iClientToShow)
{
	if(RoundStatus == RS_FreezeTime)
	{
		HUD_FreezeTime(client);
	}
	else if(RoundStatus >= RS_InProgress && !IsFinal())
	{
		if(g_bIsTribute[iClientToShow])
			HUD_Tribute(client, iClientToShow);
		else if(IsZombie(iClientToShow))
			HUD_Zombie(client, iClientToShow);
	}
}

/* HUd for Freeze Time / Countdown */

void HUD_FreezeTime(int client)
{
	int iCountDown = g_iFreezeTime - RoundToFloor(GetGameTime() - g_fRoundStart);
	
	char centerText[512];
	if(iCountDown > 0)
	{
		int iClr;
		if(iCountDown > RoundToNearest(float(g_iFreezeTime)*(0.67))) 		iClr = 0x00FF00; //Olive
		else if(iCountDown > RoundToNearest(float(g_iFreezeTime)*(0.5))) 	iClr = 0x88FF00; //Yellow
		else if(iCountDown > RoundToNearest(float(g_iFreezeTime)*(0.33))) 	iClr = 0xFFFF00; //Orange
		else if(iCountDown > RoundToNearest(float(g_iFreezeTime)*(0.17))) 	iClr = 0xFF8800; //Orange
		else 																iClr = 0xFF0000; //Red
		
		char sbuffer[128];
		Format(sbuffer, sizeof(sbuffer), "%T", "HUD_Countdown", client);
		
		char sbuffer2[128];
		Format(sbuffer2, sizeof(sbuffer2), "%T", "HUD_CountdownSeconds", client);
		
		Format(centerText, sizeof(centerText), "<font size='24' color='#FF2222'><bold>%s</bold><br><font size='24' color='#%06X'>%i %s", sbuffer, iClr, iCountDown, sbuffer2);
	}
	else Format(centerText, sizeof(centerText), "<font size='32' color='#44FF22'>GO GO GO !!!");
	
	PrintHintText(client, centerText);
}

/* HUD for Tributes */

void HUD_Tribute(int client, int iClientToShow)
{
	int iHUD = GetCurrentTributeHUD(client, iClientToShow);
	
	if(iHUD == -1)
		return;
		
	char centerText[512];
	
	/* Equipment */
	if(iHUD == 0)
	{
		Format(centerText, sizeof(centerText), "--- %T: {COUNT}---\n", "Equipment", client);
		
		Format(centerText, sizeof(centerText), "%s<font size='{SIZE}'>", centerText);
		
		int iEqLineCount = 0;
		int iEqCount = 0;
		int iLineCount = 0;
		
		if(GetThrowingKnifes(iClientToShow) > 0)
		{
			Format(centerText, sizeof(centerText), "%s%T (%d)", centerText, "Knifes", client, GetThrowingKnifes(iClientToShow));
			iEqLineCount++;
			iEqCount++;
		}
		
		if(HasCompass(client) || HasTracer(client))
		{
			if(AddSeperator(iEqLineCount, centerText))
				iLineCount++;
			
			Format(centerText, sizeof(centerText), "%s%T", centerText, HasTracer(client) ? "Tracer" : "Compass", client);
			iEqLineCount++;
			iEqCount++;
		}
		
		if(HasJammer(client))
		{
			if(AddSeperator(iEqLineCount, centerText))
				iLineCount++;
			
			Format(centerText, sizeof(centerText), "%s%T", centerText, "Jammer", client);
			iEqLineCount++;
			iEqCount++;
		}
		
		if(HasBinoculars(client))
		{
			if(AddSeperator(iEqLineCount, centerText))
				iLineCount++;
			
			Format(centerText, sizeof(centerText), "%s%T", centerText, "Binoculars", client);
			iEqLineCount++;
			iEqCount++;
		}
		
		if(HasParachute(client))
		{
			if(AddSeperator(iEqLineCount, centerText))
				iLineCount++;
			
			Format(centerText, sizeof(centerText), "%s%T", centerText, "Parachute", client);
			iEqLineCount++;
			iEqCount++;
		}
		
		Format(centerText, sizeof(centerText), "%s</font>", centerText);
		
		if(iLineCount <= 1)
			ReplaceString(centerText, sizeof(centerText), "{SIZE}", "18");
		else if(iLineCount <= 2)
			ReplaceString(centerText, sizeof(centerText), "{SIZE}", "16");
		else if(iLineCount <= 3)
			ReplaceString(centerText, sizeof(centerText), "{SIZE}", "14");
		else ReplaceString(centerText, sizeof(centerText), "{SIZE}", "12");
		
		char sCount[4];
		IntToString(iEqCount, sCount, sizeof(sCount));
		ReplaceString(centerText, sizeof(centerText), "{COUNT}", sCount);
		
		PrintHintText(client, centerText);
	}
	
	/* Compass */
	else if(iHUD == 1)
	{
		char sInfo[32];
		char sName[64];
		if(GetCompassInfo(iClientToShow, sInfo, sName) > 0)
		{
			Format(centerText, sizeof(centerText), "<font size='48'>   %s</font>%s<font size='20'>%s</font>", sInfo, StrEqual(sName, "") ? "" : ": ", sName);
			PrintHintText(client, centerText);
		}
	}
	
	/* Survival */
	
	else if(iHUD == 2)
	{
		// Hunger
		
		float hunger;
		HG_GetHunger(iClientToShow, hunger);
		
		if(hunger > 60.0) 
			Format(centerText, sizeof(centerText), "%T: <font color='#00ff00'>%T</font>", "Hunger", client, "Hunger_Full", client);
		else if(hunger > 35.0) 
			Format(centerText, sizeof(centerText), "%T: <font color='#00ff00'>%T</font>", "Hunger", client, "Hunger_Sated", client);
		else if(hunger > 15.0) 
			Format(centerText, sizeof(centerText), "%T: <font color='#ffff00'>%T</font>", "Hunger", client, "Hunger_Hungry", client);
		else if(hunger > 0.0) 
			Format(centerText, sizeof(centerText), "%T: <font color='#ffaa00'>%T</font>", "Hunger", client, "Hunger_Hungry2", client);
		else Format(centerText, sizeof(centerText), "%T: <font color='#ff0000'>%T</font>", "Hunger", client, "Hunger_Starving", client);
		
		// Thirst
		
		float thirst;
		HG_GetThirst(iClientToShow, thirst);
		
		if(thirst > 60.0) 
			Format(centerText, sizeof(centerText), "%s %T: <font color='#00ff00'>%T</font>", centerText, "Thirst", client, "Thirst_Full", client);
		else if(thirst > 45.0) 
			Format(centerText, sizeof(centerText), "%s %T: <font color='#00ff00'>%T</font>", centerText, "Thirst", client, "Thirst_Hydrated", client);
		else if(thirst > 25.0) 
			Format(centerText, sizeof(centerText), "%s %T: <font color='#ffff00'>%T</font>", centerText, "Thirst", client, "Thirst_Thirsty", client);
		else if(thirst > 10.0) 
			Format(centerText, sizeof(centerText), "%s %T: <font color='#ffaa00'>%T</font>", centerText, "Thirst", client, "Thirst_Thirsty2", client);
		else Format(centerText, sizeof(centerText), "%s %T: <font color='#ff0000'>%T</font>", centerText, "Thirst", client, "Thirst_Dehydrated", client);
		
		Format(centerText, sizeof(centerText), "%s\n\n ", centerText);
		
		// Stamina
		
		float stamina;
		HG_GetStamina(iClientToShow, stamina);
		
		if(stamina > 100.0)
			stamina = 100.0;
		
		float energy;
		HG_GetEnergy(iClientToShow, energy);
		
		if(energy > 100.0)
			energy = 100.0;
		
		// Stamina / Energy
		
		int iBadgesCount;
		int iBadgesMax = 40;
		
		int iBadgesStaminaMax = RoundToFloor(energy/2.5); 
		int iBadgesStamina = RoundToFloor(stamina/2.5);
		
		int iBadgesGap = iBadgesStaminaMax - iBadgesStamina; 
		int iBadgesLeft = iBadgesMax - iBadgesStaminaMax;
		
		// Green
		if(iBadgesStamina > 0)
		{
			Format(centerText, sizeof(centerText), "%s<font color='#00ff00'>", centerText);
			
			for (int i; i < iBadgesStamina; i++)
			{
				Format(centerText, sizeof(centerText), "%s|", centerText);
				iBadgesCount++;
			}
			
			Format(centerText, sizeof(centerText), "%s</font>", centerText);
		}
		
		// White
		if(iBadgesGap > 0)
		{
			for (int i; i < iBadgesGap; i++)
			{
				Format(centerText, sizeof(centerText), "%s|", centerText);
				iBadgesCount++;
			}
		}
		
		// Red
		if(iBadgesLeft > 0)
		{
			Format(centerText, sizeof(centerText), "%s<font color='#ff0000'>", centerText);
			
			for (int i; i < iBadgesLeft; i++)
			{
				Format(centerText, sizeof(centerText), "%s|", centerText);
				iBadgesCount++;
			}
			
			Format(centerText, sizeof(centerText), "%s</font>", centerText);
		}
		
		PrintHintText(client, centerText);
	}
}

int GetCurrentTributeHUD(int client, int iClientToShow)
{
	static int iHUD[MAXPLAYERS+1];
	
	iHUD[client]++;
	
	if(iHUD[client] > g_iHudMaxTicks)
		iHUD[client] = 0;
	
	// Compass
	if(iHUD[client] > 0 && iHUD[client] % g_iHudCompassTicks == 0 && (HasCompass(iClientToShow) || HasTracer(iClientToShow)))
	{
		if(g_bCompassAllowParty || g_iParty[client] == -1)
			return 1;
	}
	
	// Equipment
	if(iHUD[client] < g_iHudEquipmentTicks && HasEquipment(iClientToShow))
		return 0;
	
	// Survival as default
	if(g_bSurvival)
		return 2;
		
	// Equipment (is case no survival)
	if(HasEquipment(iClientToShow))
		return 0;
	
	// Nothing to show :/
	return -1;
}

/* HUD for Tributes */

void HUD_Zombie(int client, int iClientToShow)
{
	char centerText[512];
	
	char sInfo[32];
	char sName[64];
	if(GetCompassInfo(iClientToShow, sInfo, sName) > 0)
	{
		Format(centerText, sizeof(centerText), "<font size='18' color='#ff0000'>\n%T:</font>", "You_Are_A_zombie", client);
		//Format(centerText, sizeof(centerText), "<font size='12' color='#ff0000'>%T:\n</font><font size='48' color='#ff00ff'>   %s</font><font size='20' color='#ffcc11'>%s %s</font>", "You_Are_A_zombie", client, sInfo, StrEqual(sName, "") ? "" : ": ", sName);
	}
	
	PrintHintText(client, centerText);
}