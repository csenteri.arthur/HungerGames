int g_iLastEvent;

int g_iShopTarget[MAXPLAYERS + 1];
int g_iShopLastTarget[MAXPLAYERS + 1];

#include "hungergames/shop/loot.sp"
#include "hungergames/shop/events.sp"
#include "hungergames/shop/other.sp"

int GetShopTarget(int client)
{
	if(!IsValidShopTarget(g_iShopTarget[client]))
		return -1;
	
	return g_iShopTarget[client];
}

bool IsValidShopTarget(int target)
{
	return (target > 0 && IsClientInGame(target) && g_bIsTribute[target]);
}

bool CanUseShop(int client, bool event, bool msg)
{
	if(!client)
		return false;
		
	// Final knife fight
	if(IsFinal())
	{
		if(msg)
			CPrintToChat(client, "%T", "Shop_ClosedFinal", client, g_sPrefix);
		return false;
	}
	
	// Delay from roundstart
	if(GetGameTime()-g_fRoundStart < g_fShopSpawnTime)
	{
		if(msg)
			CPrintToChat(client, "%T", "Shop_ClosedSpawnTime", client, g_sPrefix, RoundToFloor(g_fShopSpawnTime), 1+RoundToFloor(g_fShopSpawnTime - (GetGameTime() - g_fRoundStart)));
		return false;
	}
	
	// Delay between events
	if(event && GetTime()-g_iLastEvent < RoundToFloor(g_fShopEventTime))
	{
		if(msg)
			CPrintToChat(client, "%T", "Shop_ClosedEventTime", client, g_sPrefix, RoundToFloor(g_fShopEventTime), 1+RoundToFloor(g_fShopEventTime - (GetTime() - g_iLastEvent)));
		return false;
	}
	
	// Alive Tribute
	if(IsPlayerAlive(client) && g_bIsTribute[client])
	{
		// Not allowed
		if(!g_bShopAlive)
		{
			CPrintToChat(client, "%T", "Shop_ClosedAlive", client, g_sPrefix);
			return false;
		}
	}
	// Are dead allowed?
	else if(!g_bShopDead || g_bShopSelfOnly)
	{
		if(msg)
			CPrintToChat(client, "%T", "Shop_ClosedDead", client, g_sPrefix);
		return false;
	}
	
	if(!g_bConnected || !g_pZcoreMysql || !client || !IsClientInGame(client) || IsFakeClient(client) || !g_bLoadedSQL[client])
	{
		if(msg)
			CPrintToChat(client, "%T", "StatsUnavailable", client, g_sPrefix);
		return false;
	}
	
	return true;
}

/* Main */

void Cmd_Shop(int client)
{
	Menu_Shop(client);
}

void Menu_Shop(int client)
{
	if(!CanUseShop(client, false, true))
		return;
	
	// Skip target selection if players can only select themselves
	if(g_bShopSelfOnly)
	{
		g_iShopTarget[client] = client;
		Menu_ShopCategory(client, true);
		return;
	}
	
	// Build the menu
	
	Menu menu = new Menu(MenuHandler_ShopPlayerSelection);
	
	menu.SetTitle("%T", "Shop_PlayerSelectionTitle", client);
	
	char sBuffer[64];
	char sTargetName[64];
	char sInfo[8];
	
	// Add self
	
	if(IsValidShopTarget(client))
	{
		Format(sBuffer, sizeof(sBuffer), "%T", "Shop_TargetSelf", client);
		IntToString(client, sInfo, sizeof(sInfo));
		menu.AddItem(sInfo, sBuffer);
	}
	
	// Add last target
	
	if(IsValidShopTarget(g_iShopLastTarget[client]) && g_iShopLastTarget[client] != client)
	{
		GetClientName(g_iShopLastTarget[client], sTargetName, sizeof(sTargetName));
		Format(sBuffer, sizeof(sBuffer), "%T", "Shop_TargetLast", client, sTargetName);
		IntToString(g_iShopLastTarget[client], sInfo, sizeof(sInfo));
		menu.AddItem(sInfo, sBuffer);
	}
	
	// Add observe target
	
	int iObserveTarget = Client_GetObservedPlayer(client);
	if(iObserveTarget > 0 && g_bIsTribute[iObserveTarget] && iObserveTarget != client && g_iShopLastTarget[client] != iObserveTarget)
	{
		GetClientName(iObserveTarget, sTargetName, sizeof(sTargetName));
		Format(sBuffer, sizeof(sBuffer), "%T", "Shop_TargetObserve", client, sTargetName);
		IntToString(iObserveTarget, sInfo, sizeof(sInfo));
		menu.AddItem(sInfo, sBuffer);
	}
	
	// Add party members
	
	int iParty = GetParty(client);
	
	if(iParty > 0)
	{
		LoopAlivePlayers(i)
		{
			if(i == client)
				continue;
				
			if(i == g_iShopLastTarget[client])
				continue;
				
			if(i == iObserveTarget)
				continue;
			
			if(!g_bIsTribute[i])
				continue;
				
			if(iParty != GetParty(i))
				continue;
				
			if(!IsValidShopTarget(i))
				continue;
			
			GetClientName(i, sTargetName, sizeof(sTargetName));
			Format(sBuffer, sizeof(sBuffer), "%T", "Shop_TargetParty", client, sTargetName);
			IntToString(i, sInfo, sizeof(sInfo));
			menu.AddItem(sInfo, sBuffer);
		}
	}
	
	// Add other players
	
	LoopAlivePlayers(i)
	{
		if(i == client)
			continue;
			
		if(i == g_iShopLastTarget[client])
			continue;
			
		if(i == iObserveTarget)
			continue;
		
		if(!g_bIsTribute[i])
			continue;
			
		if(iParty == GetParty(i) && iParty > 0)
			continue;
				
		if(!IsValidShopTarget(i))
			continue;
		
		GetClientName(i, sTargetName, sizeof(sTargetName));
		Format(sBuffer, sizeof(sBuffer), "%T", "Shop_TargetOther", client, sTargetName);
		IntToString(i, sInfo, sizeof(sInfo));
		menu.AddItem(sInfo, sBuffer);
	}
	
	SetMenuExitButton(menu, true);
	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuHandler_ShopPlayerSelection(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		if(!CanUseShop(client, false, true))
			return;
		
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		g_iShopTarget[client] = StringToInt(sInfo);
		
		if(g_iShopTarget[client] <= 0 || !IsClientInGame(g_iShopTarget[client]) || !g_bIsTribute[g_iShopTarget[client]])
		{
			CPrintToChat(client, "%T", "Shop_InvalidTarget", client, g_sPrefix);
			Menu_Shop(client);
		}
		else Menu_ShopCategory(client, true);
	}
	else if (action == MenuAction_End)
		delete menu;
}

void Menu_ShopCategory(int client, bool target)
{
	if(!CanUseShop(client, false, true))
		return;
	
	if(target && (g_iShopTarget[client] <= 0 || !IsClientInGame(g_iShopTarget[client]) || !g_bIsTribute[g_iShopTarget[client]]))
	{
		CPrintToChat(client, "%T", "Shop_InvalidTarget", client, g_sPrefix);
		return;
	}
	
	Menu menu = new Menu(MenuHandler_ShopMain);
	menu.SetTitle("%T", "Shop_CategoryTitle", client, Stats_GetPoints(client));
	
	char sBuffer[64];
	
	if(g_bShopEvents)
	{
		Format(sBuffer, sizeof(sBuffer), "%T", "Shop_Events", client);
		menu.AddItem("events", sBuffer);
	}
	
	Format(sBuffer, sizeof(sBuffer), "%T", "Shop_Loot", client);
	menu.AddItem("loot", sBuffer);
	
	Format(sBuffer, sizeof(sBuffer), "%T", "Shop_Other", client);
	menu.AddItem("other", sBuffer);
	
	SetMenuExitButton(menu, true);
	
	menu.Display(client, MENU_TIME_FOREVER);
}

public int MenuHandler_ShopMain(Menu menu, MenuAction action, int client, int params)
{
	if (action == MenuAction_Select)
	{
		if(!CanUseShop(client, false, true))
			return;
		
		char sInfo[64];
		menu.GetItem(params, sInfo, sizeof(sInfo));
		
		if(StrEqual(sInfo, "events"))
		{
			Menu_ShopEvents(client);
		}
		else if(StrEqual(sInfo, "loot"))
		{
			Menu_ShopLoot(client);
		}
		else if(StrEqual(sInfo, "other"))
		{
			Menu_ShopOther(client);
		}
	}
	else if (action == MenuAction_End)
		delete menu;
}