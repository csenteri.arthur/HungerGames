// Credits: Neuro Toxin

bool g_bPlayerIsTagged[MAXPLAYERS+1];
float g_fTaggingEndTime[MAXPLAYERS+1];

void ResetTagging(int client)
{
	g_fTaggingEndTime[client] = 0.0
	g_bPlayerIsTagged[client] = false;
}

float GetPlayerTagEndTime(int client)
{
	return g_fTaggingEndTime[client];
}

public void OnTagrenadeDetonate(Handle event, const char[] name, bool dontBroadcast)
{
	if(!g_pCustomPlayerSkins)
		return;
	
	int userid = GetEventInt(event, "userid");
	DataPack pack = new DataPack();
	pack.WriteCell(userid);
	pack.WriteCell(GetEventInt(event, "entityid"));
	pack.WriteFloat(GetEventFloat(event, "x"));
	pack.WriteFloat(GetEventFloat(event, "y"));
	pack.WriteFloat(GetEventFloat(event, "z"));
	CreateTimer(0.0, OnGetTagrenadeTimes, pack);
}

public Action OnGetTagrenadeTimes(Handle timer, any data)
{
	DataPack pack = view_as<DataPack>(data);
	pack.Reset();
	
	int client = GetClientOfUserId(pack.ReadCell());
	if (client == 0)
	{
		CloseHandle(pack);
		return Plugin_Continue;
	}
	
	int entity = pack.ReadCell();
	
	float position[3];
	float targetposition[3];
	float distance;
	
	position[0] = pack.ReadFloat();
	position[1] = pack.ReadFloat();
	position[2] = pack.ReadFloat();
	CloseHandle(pack);
	
	LoopAlivePlayers(target)
	{
		// Don't hit self
		if (client == target)
			continue;
		
		// Ignore non tributes
		if (!g_bIsTribute[target])
			continue;
			
		// Remove game tagging (not FFA compatible)
		SetEntPropFloat(target, Prop_Send, "m_flDetectedByEnemySensorTime", 0.0);
		
		// Check distance
		GetClientEyePosition(target, targetposition);
		distance = GetVectorDistance(position, targetposition);
		
		// Not in range
		if (distance > g_fTagrenadeRange)
			continue;
		
		// Skip visible check
		if(!g_bTagrenadeVisible)
		{
			TagClient(target, g_fTagrenadeTime);
			continue;
		}
		
		// Visible?
		Handle trace = TR_TraceRayFilterEx(position, targetposition, MASK_VISIBLE, RayType_EndPoint, OnTraceForTagrenade, entity);
		if (TR_DidHit(trace) && TR_GetEntityIndex(trace) == target)
			TagClient(target, g_fTagrenadeTime);
		
		CloseHandle(trace);
	}
	return Plugin_Continue;
}

void TagClient(int client, float duration)
{
	g_fTaggingEndTime[client] = GetGameTime() + duration;
}

public bool OnTraceForTagrenade(int entity, int contentsMask, any tagrenade)
{
	if (entity == tagrenade)
		return false;
	return true;
}