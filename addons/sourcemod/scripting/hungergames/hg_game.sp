Handle g_aFallenTributes = null;
Handle g_aFallenTributeNames = null;
Handle g_aFallenTributeWeapons = null;
Handle g_aFallenTributeWeaponIDs = null;
Handle g_aFallenTributeWeaponFauxIDs = null;
Handle g_aFallenTributeWeaponOwners = null;
Handle g_aFallenTributeHeadshot = null;
Handle g_aFallenTributeDominated = null;
Handle g_aFallenTributeRevenge = null;
Handle g_aFallenTributePenetrated = null;

Handle g_hFallentirbutesTimer = null;

Handle g_aDeathRelatedMessages[MAXPLAYERS + 1] =  { null, ...};

bool g_bDelayedDeath[MAXPLAYERS + 1];

void StartPlayerCheckTimer()
{
	if(g_hTimer != null)
		CloseHandle(g_hTimer);
	
	g_hTimer = CreateTimer(g_fCheckInterval, Timer_CheckPlayers, 0, TIMER_REPEAT);
}

public Action Timer_DelayedEndRound(Handle timer, any team)
{
	if(RoundStatus == RS_DelayedRoundEnd)
	{
		RoundStatus = RS_BetweenRounds;
		
		ServerCommand("mp_ignore_round_win_conditions 0");
		
		if(team == CS_TEAM_CT)
			CS_TerminateRound(0.0, CSRoundEnd_CTWin);
		else CS_TerminateRound(0.0, CSRoundEnd_TerroristWin);
	}
	
	return Plugin_Stop;
}

public Action Timer_CheckPlayers(Handle timer, any data)
{
	if(RoundStatus >= RS_FreezeTime)
		CheckPlayers();
	
	return Plugin_Continue;
}

void CheckPlayers()
{
	/* Check warmup */
	bool bIsWarmup = GameRules_GetProp("m_bWarmupPeriod") == 1;
	bool bWarmupEnded;
	if(g_bIsWarmup && !bIsWarmup)
		bWarmupEnded = true;
	g_bIsWarmup = bIsWarmup;
	
	/* Restart round if warmup has ended */
	if(bWarmupEnded)
	{
		RoundStatus = RS_DelayedRoundEnd;
		CreateTimer(1.0, Timer_DelayedEndRound, CS_TEAM_NONE, TIMER_FLAG_NO_MAPCHANGE);
		
		Call_StartForward(g_OnDelayedRoundEnd);
		Call_PushFloat(1.0);
		Call_Finish();
		
		return;
	}
	
	int iIngame;
	int iAlive;
	int iDead;
	int iLast;
	
	LoopIngameClients(client)
	{
		if(GetClientTeam(client) <= CS_TEAM_SPECTATOR)
			continue;
		
		iIngame++;
		
		if(!g_bIsTribute[client] || !IsPlayerAlive(client))
		{
			iDead++;
			continue;
		}
		
		iAlive++;
		iLast = client;
	}
	
	// Ignore if not enought players are ingame
	if(iIngame < 1)
		return;
	
	// Too much players alive
	if(iAlive > g_iEndgameMaxPlayers)
		return;
	
	// Restart round
	if(iAlive <= 1 && iDead > 0)
	{
		int iWinnerTeam;
		
		// Only one tribute alive but we had more in this round
		if(iAlive == 1 && g_iTributes > 1 && !g_bNoWinner)
		{
			Call_StartForward(g_OnRoundEnd);
			Call_PushCell(iLast);
			Call_Finish();
			
			g_iLastWinner = iLast;
			iWinnerTeam = GetClientTeam(g_iLastWinner);
		}
		
		g_bNoWinner = false;
		
		Call_StartForward(g_OnDelayedRoundEnd);
		Call_PushFloat(g_fRoundEndDelay);
		Call_Finish();
		
		RoundStatus = RS_DelayedRoundEnd;
		
		CreateTimer(g_fRoundEndDelay, Timer_DelayedEndRound, iWinnerTeam, TIMER_FLAG_NO_MAPCHANGE);
		
		g_iTributes = 0;
		return;
	}
	
	// Start Endgame
	if(RoundStatus == RS_InProgress)
	{
		float time = GetGameTime();
		float roundTime = time - g_fRoundStart;
		
		if(roundTime > g_fEndgameMinDelay)
		{
			RoundStatus = RS_Endgame;
			
			Call_StartForward(g_OnEndgameStart);
			Call_PushFloat(roundTime);
			Call_Finish();
		}
	}
}

public void HG_OnDelayedRoundEnd(float delay)
{
	ShowFallenTributesNow();
	
	int iDelay = RoundToFloor(delay);
	
	LoopIngamePlayers(i)
	{
		CPrintToChat(i, "%T", "Round_Restart", i, g_sPrefix, iDelay);
	}
	
	float tempdelay = delay-0.5;
	if (tempdelay < 0.0)
		tempdelay = 0.0;
	
	CreateTimer(tempdelay, Timer_StopFireworks, _ , TIMER_FLAG_NO_MAPCHANGE);
}

void Player_Death(int attacker, int victim, const char sWeapon[64], const char sWpItemID[64], const char sWpFauxItemID[64], const char sWpOwnerUID[64], bool hs, int dom, int rev, int pen)
{
	// Get attacker name
	char sAName[64];
	if(!Client_IsValid(attacker))
		Format(sAName, sizeof(sAName), "WORLD");
	else Format(sAName, sizeof(sAName), "%N", attacker);
	
	// Get victim name
	char sVName[64];
	if(!Client_IsValid(victim))
		Format(sVName, sizeof(sVName), "WORLD");
	else Format(sVName, sizeof(sVName), "%N", victim);
	
	if(RoundStatus >= RS_InProgress && g_bIsTribute[victim])
	{
		Call_StartForward(g_OnTributeFallen);
		Call_PushCell(victim);
		Call_Finish();
		
		DelayDeath(victim, sVName, sWeapon, sWpItemID, sWpFauxItemID, sWpOwnerUID, hs, dom, rev, pen);
	}
	
	if(victim == attacker)
	{
		CPrintToChat(victim, "%T", "Death_Selfkill", victim, g_sPrefix);
		Stats_AddPlayerDeath(victim);
	}
	else
	{
		if(Client_IsValid(victim))
		{
			CPrintToChat(victim, "%T", "Death_ByPlayer", victim, g_sPrefix, sAName);
		
			if (Client_IsValid(attacker))
			{
				if(g_fDeathDelay == 0.0)
					CPrintToChat(attacker, "%T", "Killed_Player", attacker, g_sPrefix, sVName);
				
				/* Both players are tributes, lets check for more stats*/
				if(g_bIsTribute[attacker] && g_bIsTribute[victim])
				{
					bool knife;
					if(StrContains(sWeapon, "knife") != -1 || StrContains(sWeapon, "bayonet") != -1)
						knife = true;
					
					Stats_ELO(attacker, victim, knife);
					Stats_AddKill(attacker, knife);
					Stats_AddPlayerDeath(victim);
				}
				else if(g_bIsTribute[attacker] && IsZombie(victim))
				{
					int iPoints = g_iPointsZombieKill;
					
					if(Client_HasAdminFlags(attacker, g_iVIPflags))
						iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
					
					Stats_AddPoints(attacker, iPoints, true);
				}
				else if(g_bIsTribute[victim] && IsZombie(attacker))
				{
					int iPoints = g_iPointsKillAsZombie;
					
					if(Client_HasAdminFlags(attacker, g_iVIPflags))
						iPoints = RoundToFloor(float(iPoints)*g_fPointsVIP);
					
					Stats_AddPoints(attacker, iPoints, true);
				}
			}
		}
	}
		
	g_bIsTribute[victim] = false;
	
	ResetZombie(victim);
	LeaveParty(victim);
}

public void HG_OnTributeFallen(int client)
{
	StartZombieActivateTimer();
	
	// Count Remaining Tributes
	int iAlive;
	LoopTributes(i)
		iAlive++;
	
	char sName[64];
	if(!Client_IsValid(client))
		Format(sName, sizeof(sName), "WORLD");
	else Format(sName, sizeof(sName), "%N", client);
	
	if(g_fDeathDelay == 0.0)
		LoopIngamePlayers(i)
			CPrintToChat(i, "%T", "Tribute_Fallen", i, g_sPrefix, sName, iAlive);
}

void ResetFallenTributesTimer(bool kill)
{
	if(kill && g_hFallentirbutesTimer != null)
		CloseHandle(g_hFallentirbutesTimer);
	
	g_hFallentirbutesTimer = null;
	
	if(!kill)
	{
		if(g_aFallenTributes != null)
			ClearArray(g_aFallenTributes);
			
		if(g_aFallenTributeNames != null)
			ClearArray(g_aFallenTributeNames);
			
		if(g_aFallenTributeWeapons != null)
			ClearArray(g_aFallenTributeWeapons);
			
		if(g_aFallenTributeWeaponIDs != null)
			ClearArray(g_aFallenTributeWeaponIDs);
			
		if(g_aFallenTributeWeaponFauxIDs != null)
			ClearArray(g_aFallenTributeWeaponFauxIDs);
			
		if(g_aFallenTributeWeaponOwners != null)
			ClearArray(g_aFallenTributeWeaponOwners);
			
		if(g_aFallenTributeHeadshot != null)
			ClearArray(g_aFallenTributeHeadshot);
			
		if(g_aFallenTributeDominated != null)
			ClearArray(g_aFallenTributeDominated);
			
		if(g_aFallenTributeRevenge != null)
			ClearArray(g_aFallenTributeRevenge);
			
		if(g_aFallenTributePenetrated != null)
			ClearArray(g_aFallenTributePenetrated);
	}
}

void RestartFallenTributesTimer()
{
	ResetFallenTributesTimer(true);
	
	g_hFallentirbutesTimer = CreateTimer(g_fDeathDelay, Timer_AnnounceDeadPlayers, _, TIMER_FLAG_NO_MAPCHANGE);
}

void ShowFallenTributesNow()
{
	ResetFallenTributesTimer(true);
	
	g_hFallentirbutesTimer = CreateTimer(0.0, Timer_AnnounceDeadPlayers, _, TIMER_FLAG_NO_MAPCHANGE);
}

void DelayDeath(int client, const char sName[64], const char sWeapon[64], const char sWpItemID[64], const char sWpFauxItemID[64], const char sWpOwnerUID[64], bool hs, int dom, int rev, int pen)
{
	g_bDelayedDeath[client] = true;
	
	if(g_aFallenTributes == null)
		g_aFallenTributes = CreateArray(1);
	
	if(g_aFallenTributeNames == null)
		g_aFallenTributeNames = CreateArray(64);
	
	if(g_aFallenTributeWeapons == null)
		g_aFallenTributeWeapons = CreateArray(64);
	
	if(g_aFallenTributeWeaponIDs == null)
		g_aFallenTributeWeaponIDs = CreateArray(64);
	
	if(g_aFallenTributeWeaponFauxIDs == null)
		g_aFallenTributeWeaponFauxIDs = CreateArray(64);
	
	if(g_aFallenTributeWeaponOwners == null)
		g_aFallenTributeWeaponOwners = CreateArray(64);
	
	if(g_aFallenTributeHeadshot == null)
		g_aFallenTributeHeadshot = CreateArray(1);
	
	if(g_aFallenTributeDominated == null)
		g_aFallenTributeDominated = CreateArray(1);
	
	if(g_aFallenTributeRevenge == null)
		g_aFallenTributeRevenge = CreateArray(1);
	
	if(g_aFallenTributePenetrated == null)
		g_aFallenTributePenetrated = CreateArray(1);
	
	int iSize = GetArraySize(g_aFallenTributes);
	
	//if(iSize == 0)
		//g_fFirstDeathInList = GetGameTime();
	
	PushArrayCell(g_aFallenTributes, client);
	PushArrayString(g_aFallenTributeNames, sName);
	PushArrayString(g_aFallenTributeWeapons, sWeapon);
	
	PushArrayString(g_aFallenTributeWeaponIDs, sWpItemID);
	PushArrayString(g_aFallenTributeWeaponFauxIDs, sWpFauxItemID);
	PushArrayString(g_aFallenTributeWeaponOwners, sWpOwnerUID);
	
	PushArrayCell(g_aFallenTributeHeadshot, hs);
	PushArrayCell(g_aFallenTributeDominated, dom);
	PushArrayCell(g_aFallenTributeRevenge, rev);
	PushArrayCell(g_aFallenTributePenetrated, pen);
	
	if(iSize >= 5) // Will show more if many players die at the same time
		ShowFallenTributesNow();
	else RestartFallenTributesTimer();
}

bool IsDelayedDeath(int client)
{
	return g_bDelayedDeath[client];
}

void ResetDelayedDeath(int client)
{
	g_bDelayedDeath[client] = false;
}

void DelayDeadRelatedMessage(int client, const char sMsg[512])
{
	if(g_aDeathRelatedMessages[client] == null)
		g_aDeathRelatedMessages[client] = CreateArray(512);
	
	PushArrayString(g_aDeathRelatedMessages[client], sMsg);
}

public Action Timer_AnnounceDeadPlayers(Handle timer, any data)
{
	g_hFallentirbutesTimer = null;
	
	if(g_aFallenTributeNames == null)
		return Plugin_Handled;
	
	int iSize = GetArraySize(g_aFallenTributeNames);
	
	if(iSize <= 0)
		return Plugin_Handled;
	
	// Count Remaining Tributes
	int iAlive;
	LoopTributes(i)
		iAlive++;
	
	// Announce remaining tribute count
	LoopIngamePlayers(i)
		CPrintToChat(i, "%T", "Tributes_Fallen", i, g_sPrefix, iSize, iAlive);
	
	//int iTime = RoundToFloor(GetGameTime() - g_fFirstDeathInList);
	
	// Play cannon sounds
	for (int i = 0; i < iSize; i++)
	{
		int iClient = GetArrayCell(g_aFallenTributes, i);
		
		char sWeapon[64];
		GetArrayString(g_aFallenTributeWeapons, i, sWeapon, sizeof(sWeapon));
		
		char sWeaponID[64];
		GetArrayString(g_aFallenTributeWeaponIDs, i, sWeaponID, sizeof(sWeaponID));
		
		char sWeaponFauxID[64];
		GetArrayString(g_aFallenTributeWeaponFauxIDs, i, sWeaponFauxID, sizeof(sWeaponFauxID));
		
		char sWeaponOwner[64];
		GetArrayString(g_aFallenTributeWeaponOwners, i, sWeaponOwner, sizeof(sWeaponOwner));
		
		bool hs = GetArrayCell(g_aFallenTributeHeadshot, i);
		int dom = GetArrayCell(g_aFallenTributeDominated, i);
		int rev = GetArrayCell(g_aFallenTributeRevenge, i);
		int pen = GetArrayCell(g_aFallenTributePenetrated, i);
		
		DataPack pack = new DataPack();
		CreateDataTimer(1.5*float(i), Timer_Cannon, pack, TIMER_FLAG_NO_MAPCHANGE);
		
		pack.WriteCell(iClient);
		pack.WriteCell(g_iAnnual);
		pack.WriteString(sWeapon);
		pack.WriteString(sWeaponID);
		pack.WriteString(sWeaponFauxID);
		pack.WriteString(sWeaponOwner);
		pack.WriteCell(hs);
		pack.WriteCell(dom);
		pack.WriteCell(rev);
		pack.WriteCell(pen);
		
		if(g_bDeathTeamChange && iClient && IsClientInGame(iClient) && GetClientTeam(iClient) == CS_TEAM_CT)
			CS_SwitchTeam(iClient, CS_TEAM_T);
	}
	
	LoopIngameClients(client)
	{
		g_bDelayedDeath[client] = false;
		
		if(g_aDeathRelatedMessages[client] != null)
		{
			int iMsgs = GetArraySize(g_aDeathRelatedMessages[client]);
			for (int i = 0; i < iMsgs; i++)
			{
				char sMsg[512];
				GetArrayString(g_aDeathRelatedMessages[client], i, sMsg, sizeof(sMsg));
				
				if(IsClientInGame(client))
					CPrintToChat(client, sMsg);
			}
			ClearArray(g_aDeathRelatedMessages[client]);
		}
	}
	
	ClearArray(g_aFallenTributes);
	ClearArray(g_aFallenTributeNames);
	ClearArray(g_aFallenTributeWeapons);
	
	return Plugin_Handled;
}

public Action Timer_Cannon(Handle timer, Handle pack)
{
	ResetPack(pack);
	
	int victim = ReadPackCell(pack);
	int iAnnual = ReadPackCell(pack);
	
	char sWeapon[64];
	ReadPackString(pack, sWeapon, sizeof(sWeapon));
		
	char sWeaponID[64];
	ReadPackString(pack, sWeaponID, sizeof(sWeaponID));
		
	char sWeaponFauxID[64];
	ReadPackString(pack, sWeaponFauxID, sizeof(sWeaponFauxID));
	
	char sWeaponOwner[64];
	ReadPackString(pack, sWeaponOwner, sizeof(sWeaponOwner));
	
	bool hs = ReadPackCell(pack);
	int dom = ReadPackCell(pack);
	int rev = ReadPackCell(pack);
	int pen = ReadPackCell(pack);
	
	if(victim && IsClientInGame(victim) && IsClientConnected(victim) && g_iAnnual == iAnnual && RoundStatus >= RS_InProgress)
	{
		if(g_bSndCannon)
			EmitSoundToAllAny(sndCannon);
		
		Handle event = CreateEvent("player_death", true);
		
		if(event != null)
		{
			g_bShowDeathMsg[victim] = true;
			
			SetEventInt(event, "userid", GetClientUserId(victim));
			
			int attacker = g_iKiller[victim];
			
			// Show as selfkill if the attacker is not in game anymore
			if(!attacker || !IsClientInGame(attacker) || !IsClientConnected(attacker))
				attacker = victim;
			
			SetEventInt(event, "attacker", GetClientUserId(attacker));
			
			SetEventString(event, "weapon", sWeapon);
			
			SetEventString(event, "weapon_itemid", sWeaponID);
			SetEventString(event, "weapon_fauxitemid", sWeaponFauxID);
			SetEventString(event, "weapon_originalowner_xuid", sWeaponOwner);
			
			SetEventBool(event, "headshot", hs);
			SetEventInt(event, "dominated", dom);
			SetEventInt(event, "revenge", rev);
			SetEventInt(event, "penetrated", pen);
			
			FireEvent(event);
		}
	}
	
	return Plugin_Handled;
}

public void Hook_OnThinkPost_Players(int iEntity)
{
	/* Alive status */
	
	int iAlive[MAXPLAYERS + 1] = {1, ...}; //TODO
	
	static int iAliveOffset = -1;
	if (iAliveOffset == -1)
		iAliveOffset = FindSendPropInfo("CCSPlayerResource", "m_bAlive");
	
	SetEntDataArray(iEntity, iAliveOffset, iAlive, MaxClients+1);
	
	/* Show total kills as kills */
	
	static int iKillOffset = -1;
	if (iKillOffset == -1)
		iKillOffset = FindSendPropInfo("CCSPlayerResource", "m_iKills");
	
	SetEntDataArray(iEntity, iKillOffset, g_iKills, MaxClients+1);
	
	/* Show total death as death */ 
	
	static int iDeathOffset = -1;
	if (iDeathOffset == -1)
		iDeathOffset = FindSendPropInfo("CCSPlayerResource", "m_iDeaths");
	
	SetEntDataArray(iEntity, iDeathOffset, g_iDeaths, MaxClients+1);
	
	/* Show points as assist */ 
	
	static int iAssistsOffset = -1;
	if (iAssistsOffset == -1)
		iAssistsOffset = FindSendPropInfo("CCSPlayerResource", "m_iAssists");
	
	SetEntDataArray(iEntity, iAssistsOffset, g_iPoints, MaxClients+1);
	
	/* Show ELO as score */
	
	static int iScoreOffset = -1;
	if (iScoreOffset == -1)
		iScoreOffset = FindSendPropInfo("CCSPlayerResource", "m_iScore");
	
	SetEntDataArray(iEntity, iScoreOffset, g_iElo, MaxClients+1);
}

/* Natives */

public int Native_GetRoundState(Handle plugin, int numParams)
{
	return view_as<int>(RoundStatus);
}

public int Native_GetLastRoundWinner(Handle plugin, int numParams)
{
	return g_iLastWinner;
}

public int Native_IsTribute(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	return g_bIsTribute[client];
}

public int Native_SetTribute(Handle plugin, int numParams)
{
	int client = GetNativeCell(1);
	bool status = GetNativeCell(2);
	
	g_bIsTribute[client] = status;
}